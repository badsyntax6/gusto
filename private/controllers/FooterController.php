<?php 

/**
 * Footer Controller Class
 * 
 * The FooterController handles logic specific to the footer and displays the 
 * footer view. The FooterController should be loaded in each controller class 
 * where a footer is desired.
 */
class FooterController extends Controller
{
    public function index()
    {     
        return Load::view('common/footer', null);
    }
}