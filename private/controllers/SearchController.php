<?php 

/**
 * Search Controller Class
 *
 * This SearchController class interacts with the SearchModel to find results 
 * in the database matching the search string and pushes them to the view.
 */
class SearchController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //root/search
     * @link //root/search/index
     * 
     * Search the database and output the found results as json.
     * 
     * @param string $string String to be searched 
     */
    public function index()
    {   
        $users = [];

        if (!empty($_POST['string'])) {
            $string = $_POST['string'];
            $model = Load::model('search');
            $users = $model->searchUsers($string);
        }

        $output = ['users' => $users];

        Output::json($output);
    }
}