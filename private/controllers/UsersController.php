<?php 

/**
 * Users Controller Class
 *
 * This class gets users information and has the ability to alter a users status.
 */
class UsersController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //root/users
     * @link //root/users/index
     *
     * This method will load the users list table.
     */
    public function index()
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['pagination'] = Load::view('common/pagination');
        $view['controls'] = Load::view('users/controls', ['group' => Auth::group()]);

        Output::html('users/view', $view);
    }

    /**
     * Get paginated records for a table
     * 
     * Get user records, paginate them, and output them as json. This method is tightly
     * coupled with the pagination javascript file.
     * 
     * @return void
     * 
     * @see /root/public/javascript/pagination.js
     * @see /root/private/models/PaginationModel.php
     */
    public function getTable()
    {
        $settings['table'] = 'user'; 
        $settings['orderby'] = 'user_id'; 
        $settings['direction'] = 'asc'; 
        $settings['page'] = 1; 
        $settings['limit'] = 15; 
        $settings['search_keys'] = ['firstname', 'lastname', 'email', 'group']; 
        $settings['search_string'] = '';

        $settings = rewriteArray($settings, $_POST);

        if (isset($_POST['search_string'])) {
            if (stripos($_POST['search_string'], 'locked') !== false) $settings['search_string'] = '0';
            if (stripos($_POST['search_string'], 'pending') !== false) $settings['search_string'] = '1';
            if (stripos($_POST['search_string'], 'registered') !== false) $settings['search_string'] = '2';
            if (stripos($_POST['search_string'], 'moderator') !== false) $settings['search_string'] = '3';
            if (stripos($_POST['search_string'], 'admin') !== false) $settings['search_string'] = '4';
        }

        if ($settings['orderby'] == 'status') $settings['orderby'] = 'last_active';
        if ($settings['orderby'] == 'name') $settings['orderby'] = 'firstname';

        $users = Load::model('user')->getUsersForPagination($settings);
        $paginated = paginate($users);

        $view['users'] = [];

        foreach ($paginated['records'] as $user) {
            switch ($user['group']) {
                case '2': $group = 'Registered'; break;
                case '3': $group = 'Moderator'; break;
                case '4': $group = 'Admin'; break;
                case '0': $group = 'Locked'; break;
                default: $group = 'Pending'; break;
            }
            
            $view['users'][] = [
                'user_id' => $user['user_id'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'signup_date' => date('d M, Y', strtotime($user['signup_date'])),
                'status' => $this->userOnline($user['last_active']) ? 'Online' : 'Offline',
                'group' => $group,
                'group_num' => $user['group']
            ];
        }

        $output = [
            'list' => Load::view('users/table', $view),
            'table' => $settings['table'],
            'orderby' => $settings['orderby'],
            'direction' => $settings['direction'],
            'limit' => $settings['limit'],
            'page' => $settings['page'],
            'start' => $paginated['start'],
            'total_pages' => $paginated['pages'],
            'total_records' => $paginated['total'],
            'search_keys' => $settings['search_keys'],
            'search_string' => $settings['search_string']
        ];

        Output::json($output);
    }

    /**
     * Get and display user data
     *
     * @param string $name - users name
     * 
     * @return void
     */
    public function user($name = null)
    {       
        if (is_null($name)) Load::route('/users');

        $name = explode('-', $name);
        $firstname = $name[0];
        $lastname = $name[1];
        $user = Load::model('user')->getUserProfile($firstname, $lastname);
        
        if (!$user) Load::route('/users');

        $value_online = $this->userOnline($user['last_active']);
        $la_days_ago = getTimeAgo($user['last_active'], false);
        $sd_days_ago = getTimeAgo($user['signup_date'], false);

        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();

        if ($user['user_id'] == Auth::id()) {
            $view['self'] = true;
        } else {
            $view['self'] = null;
        }

        $view['user_id'] = $user['user_id'];
        switch ($user['group']) {
            case 0: $view['group'] = 'Locked'; break;
            case 1: $view['group'] = 'Activation pending'; break;
            case 2: $view['group'] = 'Registered'; break;
            case 3: $view['group'] = 'Moderator'; break;
            case 4: $view['group'] = 'Administrator'; break;
        }
        $view['firstname'] = $user['firstname'];
        $view['lastname'] = $user['lastname'];
        $view['username'] = $user['username'];
        $view['email'] = $user['email'];
        $view['phone'] = $user['phone'];
        $view['country'] = $user['country'];
        $view['birthday'] = isset($user['birthday']) ? date('d M, Y', strtotime($user['birthday'])) : '';
        $view['gender'] = $user['gender'];
        $view['bio'] = $user['bio'];
        $view['registered'] = date('d M, Y', strtotime($user['signup_date']));
        $view['last_active'] = isset($user['last_active']) ? date('d M, Y', strtotime($user['last_active'])) : 'Never';
        if ($user['privacy'] == 0) $view['privacy'] = 'Public'; 
        if ($user['privacy'] == 1) $view['privacy'] = 'Private';
        if ($user['privacy'] == 2) $view['privacy'] = 'Locked';
        $view['avatar'] = $user['avatar'];
        $view['status'] = $value_online ? 'Online' : 'Offline';
        if ($la_days_ago !== false) {
            $view['la_days_ago'] = $la_days_ago == '0' ? '(Today)' : '(' . $la_days_ago . ')';
        } else {
            $view['la_days_ago'] = '';
        }
        $view['sd_days_ago'] = '(' . $sd_days_ago . ')';

        Output::html('users/user', $view);
    }

    /**
     * Check if the user is online
     * 
     * If the users last activity was more than 5 minutes ago we assume they are no longer online.
     *
     * @param string $last_active Date the user was last active.
     * 
     * @return bool
     */
    private function userOnline($last_active) 
    {
        $last_active = strtotime($last_active);

        if (time() - $last_active > 5 * 60) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Edit a user
     * 
     * This method really only changes the users group level. So its mostly
     * used to activate and ban users... mostly.
     *
     * @return void
     */
    public function edit()
    {
        $model = Load::model('user');
        $group = $_POST['group'];
        $ids = explode(',', $_POST['ids']);

        foreach ($ids as $id) {
            $user = $model->getWhere('user_id', $id);

            if ($user) {
                switch ($group) {
                    case '0': $group_text = 'Locked'; break;
                    case '1': $group_text = 'Un-registered'; break;
                    case '2': $group_text = 'Registered'; break;
                    case '3': $group_text = 'Moderator'; break;
                    case '4': $group_text = 'Administrator'; break;
                    default: $group_text = 'Unknown'; break;
                }

                $data['group'] = $group;
                $data['user_id'] = $id;

                $output['group'] = $group;

                $log = ['admin' => Auth::firstname() . ' ' . Auth::lastname(), 'name' => $user['firstname'] . ' ' . $user['lastname'], 'group' => $group_text];

                if ($model->updateBy('user_id', $data)) {
                    $output = ['alert' => 'success', 'message' => Language::get('users/user_updated'), 'group' => $group_text];
                    Log::event(Language::get('users/log_user_update', $log));
                } else {
                    $output = ['alert' => 'error', 'message' => Language::get('users/user_not_updated')];
                    Log::event(Language::get('users/log_user_update_fail', $log));
                }
            }
        }

        Output::json($output);
    }

    /**
     * Delete a user
     * 
     * This method will completely remove a user from the database.
     * This is not a soft delete, they will be gone.
     *
     * @return void
     */
    public function delete()
    {
        $model = Load::model('user');
        $ids = explode(',', $_POST['ids']);

        foreach ($ids as $id) {
            $user = $model->getWhere('user_id', $id);
            if ($user && $user['user_id'] != 1) {
                if ($model->deleteWhere('user_id', $id)) {
                    $output = ['alert' => 'success', 'message' => Language::get('users/users_deleted')];
                    Log::event('Admin "' . Auth::firstname() . ' ' . Auth::lastname() .  '" deleted user "' . $user['firstname'] . ' ' . $user['lastname'] . '".');
                } else {
                    $output = ['alert' => 'error', 'message' => 'User delete failed.'];
                    Log::event('Admin "' . Auth::firstname() . ' ' . Auth::lastname() .  '" was unable to delete user "' . $user['firstname'] . ' ' . $user['lastname'] . '". Check error logs.');
                }
            }   
        }

        Output::json($output);
    }

    public function getUserNumbers() {
        $users = Load::model('user');
        $total_users = $users->countAll();
        $locked = $users->countWhere('group', 0);
        $pending = $users->countWhere('group', 1);
        $registered = $users->countWhere('group', 2);
        $mods = $users->countWhere('group', 3);
        $admins = $users->countWhere('group', 4);

        $data['total'] = $total_users ? $total_users : 0;
        $data['locked'] = $locked ? $locked : 0;
        $data['pending'] = $pending ? $pending : 0;
        $data['registered'] = $registered ? $registered : 0;
        $data['mods'] = $mods ? $mods : 0;
        $data['admins'] = $admins ? $admins : 0;

        Output::json($data);
    }
}