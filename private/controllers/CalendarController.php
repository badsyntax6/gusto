<?php

/**
 * Calendar Controller Class
 * 
 * 
 * This Controller is tightly coupled with the calendar library and
 * the calendar javascript file.
 * 
 * @see Calendar Library Class - /root/private/libraries/Calendar.php 
 * @see Calendar JS - /root/public/javascript/calendar.php 
 */
class CalendarController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //root/calendar
     * @link //root/calendar/index
     * 
     * Draw the calendar view.
     */
    public function index()
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();

        Output::html('calendar/calendar', $view);
    }

    /**
     * Draw the new calendar event view.
     *
     * @return void
     */
    public function add() 
    {
        $view['edit'] = false;
        Output::html('calendar/new', $view);
    }

    /**
     * Save a new calendar event
     *
     * @return void
     */
    public function save()
    {
        $data['title'] = $_POST['title'];
        $data['start_time'] = date('c', strtotime($_POST['start_date']));
        $data['end_time'] = !empty($_POST['end_date']) ? date('c', strtotime($_POST['end_date'])) : date('c', strtotime($_POST['start_date']));
        $data['description'] = $_POST['description'];
        $data['color'] = !empty($_POST['color']) ? $_POST['color'] : '#b9dae2';

        if (Load::model('calendar')->save($data)) {
            $output = ['alert' => 'success', 'message' => Language::get('calendar/event_saved')];
        } else {
            $output = ['alert' => 'error', 'message' => Language::get('calendar/event_saved_fail')];
        }

        Output::json($output);
    }

    /**
     * Update the calendar event
     *
     * @return void
     */
    public function update()
    {
        $data['title'] = $_POST['title'];
        $data['start_time'] = date('c', strtotime($_POST['start_date']));
        $data['end_time'] = date('c', strtotime($_POST['end_date']));
        $data['description'] = $_POST['description'];
        $data['color'] = !empty($_POST['color']) ? $_POST['color'] : '#b9dae2';
        $data['event_id'] = $_POST['event_id'];

        if (Load::model('calendar')->updateBy('event_id', $data)) {
            $output = ['alert' => 'success', 'message' => Language::get('calendar/event_updated')];
        } else {
            $output = ['alert' => 'error', 'message' => Language::get('calendar/event_update_fail')];
        }

        Output::json($output);
    }

    /**
     * Get event data in json format
     * 
     * This method is almost identical to the getEvent method in this class 
     * and the two should probably be combined at some point. This one is used 
     * when editing a calendar evetn. Dates for the date input on the form 
     * must be formated a specific way to be filled by js.
     *
     * @param int $id
     * @return void
     */
    public function getEventJson($id)
    {
        $event = Load::model('calendar')->getWhere('event_id', $id);

        $event['start_date'] = date('Y-m-d', strtotime($event['start_time'])); 
        $event['end_date'] = date('Y-m-d', strtotime($event['end_time']));
        $event['start_time'] = date('H:i', strtotime($event['start_time']));
        $event['end_time'] = date('H:i', strtotime($event['end_time']));
        $event['color'] = isset($event['color']) ? $event['color'] : '#142f36';

        Output::json($event);
    }

    /**
     * Get the calendar data
     *
     * Retrieve all the data that makes up the calendar from the calendar library.
     * 
     * @see Calendar Library Class - /root/private/library/Calendar.php 
     * @return void
     */
    public function getCalendar()
    {
        $date = !empty($_POST['date']) ? $_POST['date'] : null;
        $calendar = Load::library('calendar');
        $calendar = $calendar->getCalendar($date);

        Output::json($calendar);
    }

    /**
     * Get the calendar events
     * 
     * This is for multiple events. Get all this months calendar events and
     * spit them out in json format. It also gets the previous months events
     * in case any of them bleed over into this month.
     *
     * @return void
     */
    public function getEvents()
    {
        $model = Load::model('calendar');
        $date = !empty($_POST['date']) ? $_POST['date'] : null;
        $timestamp = strtotime($date);
        $lm = date('j M Y', strtotime('first day of last month', $timestamp));
        $events = $model->getEvents($date);
        $lm_events = $model->getEvents($lm);
        $calendar = Load::library('calendar');
        $calendar = $calendar->getCalendar($date);
        $output = [];

        if ($events) {
            foreach ($events as $e) {
                $output['data'][] = [
                    'id' => $e['event_id'],
                    'title' => $e['title'],
                    'description' => $e['description'],
                    'color' => isset($e['color']) ? $e['color'] : '',
                    'start_day' => date('j', strtotime($e['start_time'])),
                    'end_day' => date('j', strtotime($e['end_time'])),
                    'start' => strtotime($e['start_time']),
                    'start_month' => date('M Y', strtotime($e['start_time'])),
                    'start_month_iso' => date('Y-m', strtotime($e['start_time'])),
                    'end_month' => date('M Y', strtotime($e['end_time'])),
                    'end_month_iso' => date('Y-m', strtotime($e['end_time'])),
                    'start_time_short' => date('ga', strtotime($e['start_time'])),
                    'start_time' => date('g:ia', strtotime($e['start_time'])),
                    'start_time_mil' => date('G', strtotime($e['start_time'])),
                    'end_time_short' => date('ga', strtotime($e['end_time'])),
                    'end_time' => date('g:ia', strtotime($e['end_time'])),
                    'end_time_mil' => date('G', strtotime($e['end_time'])),
                    'day_count' => $calendar['day_count'],
                    'this_month' => date('m', strtotime($calendar['month_num'])),
                    'last_month' => date('M Y', strtotime($calendar['last_month'])),
                    'last_month_iso' => date('Y-m', strtotime($calendar['last_month'])),
                    'today' => date('j', strtotime($calendar['date']))
                ];
            }
        }

        if ($lm_events) {
            foreach ($lm_events as $e) {
                $end_month = date('M Y', strtotime($e['end_time']));
                if ($end_month == $calendar['month_year']) {
                    $output['data'][] = [
                        'id' => $e['event_id'],
                        'title' => $e['title'],
                        'description' => $e['description'],
                        'color' => isset($e['color']) ? $e['color'] : '',
                        'start_day' => date('j', strtotime($e['start_time'])),
                        'end_day' => date('j', strtotime($e['end_time'])),
                        'start' => strtotime($e['start_time']),
                        'start_month' => date('M Y', strtotime($e['start_time'])),
                        'start_month_iso' => date('Y-m', strtotime($e['start_time'])),
                        'start_date' => date('Y-m', strtotime($e['start_time'])),
                        'end_month' => date('M Y', strtotime($e['end_time'])),
                        'end_month_iso' => date('Y-m', strtotime($e['end_time'])),
                        'start_time_short' => date('ga', strtotime($e['start_time'])),
                        'start_time' => date('g:ia', strtotime($e['start_time'])),
                        'start_time_mil' => date('G', strtotime($e['start_time'])),
                        'end_time_short' => date('ga', strtotime($e['end_time'])),
                        'end_time' => date('g:ia', strtotime($e['end_time'])),
                        'end_time_mil' => date('G', strtotime($e['end_time'])),
                        'day_count' => $calendar['day_count'],
                        'last_month' => date('M Y', strtotime($calendar['last_month'])),
                        'last_month_iso' => date('Y-m', strtotime($calendar['last_month'])),
                        'today' => date('j', strtotime($calendar['date']))
                    ];
                }
            }
        }

        if (isset($output['data'])) {
            usort($output['data'], function ($a, $b) {
                return strtotime($a['start_time']) - strtotime($b['start_time']);
            });
        }

        Output::json($output);
    }

    /**
     * Get event in json format
     * 
     * This is for a single event. This method is almost identical to the getEvenJson method in this class 
     * and the two should probably be combined at some point. This one is used 
     * to pop up the event when one is clicked in the main calendar view.
     *
     * @return void
     */
    public function getEvent()
    {
        $id = $_POST['id'];
        $e = Load::model('calendar')->getWhere('event_id', $id);

        $data['title'] = $e['title'];
        $data['description'] = $e['description'];
        $data['start_time'] = date('M j, Y g:ia', strtotime($e['start_time']));
        $data['end_time'] = date('M j, Y g:ia', strtotime($e['end_time']));

        Output::json($data);
    }

    /**
     * Delete a calendar event
     *
     * @return void
     */
    public function deleteEvent()
    {
        $id = $_POST['id'];

        if (Load::model('calendar')->deleteWhere('event_id', $id)) {
            $output = ['alert' => 'success', 'message' => 'Successfulness'];
        } else {
            $output = ['alert' => 'error', 'message' => 'failfullness'];
        }

        Output::json($output);
    }
}
