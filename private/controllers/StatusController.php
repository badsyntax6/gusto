<?php 

/**
 * Status Controller Class
 */
class StatusController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //root/not-found
     * @link //root/not-found/index
     */
    public function index($code = null)
    {   
        $view['header'] = Load::controller('header')->index($code);
        $view['footer'] = Load::controller('footer')->index();

        switch ($code) {
            // 100
            case 100: $view['code'] = Language::get('status/100'); break;
            case 101: $view['code'] = Language::get('status/101'); break;
            case 102: $view['code'] = Language::get('status/102'); break;
            case 103: $view['code'] = Language::get('status/103'); break;
            // 200
            case 200: $view['code'] = Language::get('status/200'); break;
            case 201: $view['code'] = Language::get('status/201'); break;
            case 202: $view['code'] = Language::get('status/202'); break;
            case 203: $view['code'] = Language::get('status/203'); break;
            case 204: $view['code'] = Language::get('status/204'); break;
            case 205: $view['code'] = Language::get('status/205'); break;
            case 206: $view['code'] = Language::get('status/206'); break;
            case 207: $view['code'] = Language::get('status/207'); break;
            case 208: $view['code'] = Language::get('status/208'); break;
            case 226: $view['code'] = Language::get('status/226'); break;
            // 300
            case 300: $view['code'] = Language::get('status/300'); break;
            case 301: $view['code'] = Language::get('status/301'); break;
            case 302: $view['code'] = Language::get('status/302'); break;
            case 303: $view['code'] = Language::get('status/303'); break;
            case 304: $view['code'] = Language::get('status/304'); break;
            case 305: $view['code'] = Language::get('status/305'); break;
            case 306: $view['code'] = Language::get('status/306'); break;
            case 307: $view['code'] = Language::get('status/307'); break;
            case 308: $view['code'] = Language::get('status/308'); break;
            // 400
            case 400: $view['code'] = Language::get('status/400'); break;
            case 401: $view['code'] = Language::get('status/401'); break;
            case 402: $view['code'] = Language::get('status/402'); break;
            case 403: $view['code'] = Language::get('status/403'); break;
            case 404: $view['code'] = Language::get('status/404'); break;
            case 405: $view['code'] = Language::get('status/405'); break;
            case 406: $view['code'] = Language::get('status/406'); break;
            case 407: $view['code'] = Language::get('status/407'); break;
            case 408: $view['code'] = Language::get('status/408'); break;
            case 409: $view['code'] = Language::get('status/409'); break;
            case 410: $view['code'] = Language::get('status/410'); break;
            case 411: $view['code'] = Language::get('status/411'); break;
            case 412: $view['code'] = Language::get('status/412'); break;
            case 413: $view['code'] = Language::get('status/413'); break;
            case 414: $view['code'] = Language::get('status/414'); break;
            case 415: $view['code'] = Language::get('status/415'); break;
            // 500
            case 500: $view['code'] = Language::get('status/500'); break;
            case 501: $view['code'] = Language::get('status/501'); break;
            case 502: $view['code'] = Language::get('status/502'); break;
            case 503: $view['code'] = Language::get('status/503'); break;
            case 504: $view['code'] = Language::get('status/504'); break;
            case 505: $view['code'] = Language::get('status/505'); break;
            default: $view['code'] = Language::get('status/unknown'); break;
        }

        Output::html('information/status', $view);  
    }
}