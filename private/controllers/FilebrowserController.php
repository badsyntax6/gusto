<?php 

/**
 * Filebrowser Controller Class
 * 
 * This controller is tightly coupled with the filebrowser library. It handles input 
 * from the user and displays output to the user. It is also tightly coupled with
 * the filebrowser javascript file.
 * 
 * @see Filebrowser Library Class - /root/private/libraries/Filebrowser.php
 * @see Filebrowser JS - /root/public/javascript/filebrowser.php
 */
class FilebrowserController extends Controller
{
    public function index()
    {
        $view['header'] = Load::controller('header')->index('Filebrowser');
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();

        Output::html('common/filebrowser', $view);
    }

    /**
     * Browse a directory
     *
     * @return void
     */
    public function browse()
    {
        $result = Load::library('filebrowser')->browse();
        Output::json($result);
    }

    /**
     * Make a new folder
     *
     * @return void
     */
    public function makeFolder()
    {
        $foldername = $_POST['foldername'];
        $directory = PUBLIC_DIR . $_POST['dir'];
        $filebrowser = Load::library('filebrowser');

        $filebrowser->makeFolder($foldername, $directory);

        if ($filebrowser->error && $filebrowser->error == 'name_invalid') {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $filebrowser->error)]));
        }

        if ($filebrowser->error && $filebrowser->error == 'dir_exists') {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $filebrowser->error)]));
        }

        exit(Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/folder_created')]));
    }

    public function delete() 
    {
        $directory = PUBLIC_DIR . $_POST['dir'];
        $files = $_POST['filenames'];
        foreach ($files as $f) {
            $filebrowser = Load::library('filebrowser')->delete($directory, $f);
            if ($filebrowser->system_file) return Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/system_file')]);
        }
        if ($filebrowser->file_deleted) Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/file_deleted')]);
    }

    public function upload()
    {
        $upload_dir = $_POST['upload_dir'];
        $upload = Load::library('Upload');
        $image = Load:: library('Image');
        $is_image = getimagesize($_FILES['upload_item']['tmp_name']) ? true : false;

        $upload->uploadAnything($_FILES['upload_item'], $upload_dir, 1000000);
        
        if ($upload->error && $upload->error == 'name_invalid') {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $upload->error)]));
        }
        if ($upload->error && $upload->error == 'file_exists') {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $upload->error, ['name' => $_FILES['upload_item']['name']])]));
        }
        if ($upload->error && $upload->error == 'extension_invalid') {
            if ($is_image) exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $upload->error, ['types' => 'jpg | jpeg | png | gif | tif | tiff | svg'])]));
            if (!$is_image) exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $upload->error, ['types' => 'txt | pdf | doc | docx | xls | xml | zip | tar | 7z | ppt'])]));
        }
        if ($upload->error && $upload->error == 'too_big') exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/' . $upload->error, ['size' => $upload->filesize, 'maxsize' => $upload->max_size])]));
        if ($source_image = $upload->success) {
            if ($is_image) $image->makeThumbnail($source_image['source'], $source_image['dir'], 100);
            Log::event(Auth::fullname() . ' uploaded images "' . $_FILES['upload_item']['name'] . '".');

            exit(Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/file_uploaded')]));
        }
    }
}