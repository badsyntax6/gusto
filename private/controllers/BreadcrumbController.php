<?php 

/**
 * Breadcrumb Controller Class
 *
 * This class creates a breadcrumb array for use in the view.
 */
class BreadcrumbController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * no route access to this controller
     *
     * This index method will create an array using the url. This method will also
     * create a breadcrumb view. The breadcrumb can be overridden for a custom breadcrumb.
     * If the crumbs parameter is set, the normal breadcrumb will be overwritten with the 
     * custom breadcrumb provided in the $crumbs parameter. 
     * 
     * @example, An override breadcrumb is created by the programmer as an array, and an example is below.
     * $crumbs = [['link' => 'user', 'crumb' => 'user'], ['link' => 'user/bilbo', 'crumb' => 'Bilbo']]
     * 
     * When you call the breadcrumb controller feed it the array.
     * $view['breadcrumb'] = Load::controller('breadcrumb')->index($crumbs);
     * 
     * Expected output 
     * USER > BILBO
     * 
     * @param array $crumbs - Multidimensional array of paths and crumb names.
     * @return void
     */
    public function index($crumbs = null)
    {
        $view['breadcrumbs'] = [];

        if (is_null($crumbs)) {
           
            $links = isset($_GET['url']) ? explodeUrl($_GET['url']) : null;
            $paths = [];

            if ($links[0] == 'dashboard') unset($links[0]);
            
            while (!empty($links)) {
                array_push($paths, implode('/', $links));
                array_pop($links);
            }

            sort($paths);

            foreach ($paths as $path) {
                $path_array = explode('/', $path);

                $view['breadcrumbs'][] = [
                    'link' => $path,
                    'crumb' => str_replace('-', ' ', strtoupper(end($path_array)))
                ];
            }
        }

        if (isset($crumbs)) {
            foreach ($crumbs as $crumb) {
                array_push($view['breadcrumbs'], $crumb);
            }
        }

        array_unshift($view['breadcrumbs'], ['link' => 'dashboard', 'crumb' => '<i class="fas fa-home fa-fw"></i>']);

        return Load::view('common/breadcrumb', $view);
    }
}