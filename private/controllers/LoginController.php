<?php 

/**
 * Login Controller Class
 *
 * The login controller class validates user logins and creates sessions to 
 * track users that have logged in. It also tracks failed login attempts.
 */
class LoginController extends Controller
{
    /**
     * Init method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //gusto/login
     * @link //gusto/login/index
     */
    public function index()
    {     
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();

        if (Auth::isLogged()) Load::route('/dashboard');

        Output::html('account/login', $view);
    }

    /**
     * Validate the login form
     *
     * The post data is submitted by ajax in the login view. If the data is 
     * valid this method will also access the session library to create a 
     * session and log the user in.
     */
    public function validate()
    {   
        botTest($_POST['red_herring']);

        $route = Session::getCookie('desired_route');

        $this->email = Sanitize::email($_POST['email']);
        $this->password = $_POST['password'];
        $this->route = $route ? $route : '/dashboard';
        $this->user = Load::model('user')->getWhere('email', $this->email);
        $this->time = date('c');
        $this->ip = $_SERVER['REMOTE_ADDR'];

        if (!$this->user) {
            Log::event(str_replace('{{email}}', $this->email, Language::get('login/log_unregistered')));
            exit(Output::json(['alert' => 'error', 'message' => Language::get('login/login_fail')]));
        }

        if ($this->ipIsBanned() || $this->userIsBanned()) {
            Log::event(str_replace('{{email}}', $this->email, Language::get('login/log_locked')));
            exit(Output::json(['alert' => 'error', 'message' => Language::get('login/locked')]));
        }

        if ($this->activationPending()) {
            Log::event(str_replace('{{email}}', $this->email, Language::get('login/log_unactivated')));
            exit(Output::json(['alert' => 'error', 'message' => Language::get('login/activation_pending')]));
        }

        if (!password_verify($this->password, $this->user['password'])) {
            $this->logAttempts();
            exit(Output::json(['alert' => 'error', 'message' => Language::get('login/login_fail')]));
        }

        $data['ip'] = $this->ip;
        $data['user_id'] = $this->user['user_id'];

        Load::model('user')->updateBy('user_id', $data);
        Load::model('user', 'logins')->deleteWhere('ip', $this->ip);

        Session::create('id', $this->user['key']);

        if (Auth::isLogged()) {
            Log::event(Language::get('login/login_success', ['name' => Auth::firstname() . ' ' . Auth::lastname()]));
            $output = ['alert' => 'success', 'route' => $this->route];
        } else {
            $output = ['alert' => 'error', 'message' => 'Dunno.'];
        }

        Output::json($output);
    }

    /**
     * Log failed login attempts
     * 
     * When a user attempts to log in and fails. Make a db record of the email address 
     * used their ip address and the number of times they tried to log in. If this is 
     * not the users first try the number of attempts will increase by 1.
     *
     * @return void
     */
    private function logAttempts()
    {
        $data['email'] = $this->email;
        $data['lock_time'] = $this->time;
        $data['ip'] = $this->ip;

        $attempts = Load::model('user')->getLoginAttempts($this->ip, $this->email);

        if (!$attempts) {  
            $data['attempts'] = 1;
            Load::model('user', 'logins')->save($data);
        } else {
            $data['attempts'] = ++$attempts['attempts'];
            Load::model('user')->updateLoginAttempts($data);
        }    
        
        Log::event(Language::get('login/log_attempt', ['email' => $this->email, 'attempts' => $data['attempts']]));
    }

    /**
     * Check if the ip is banned
     * 
     * If a user with $this->ip has attempted to login more 
     * than 10 times and failed, They are considered locked.
     * Used in $this->validate();
     * 
     * @see $this->validate();
     * @return bool
     */
    private function ipIsBanned() 
    {
        $attempts = Load::model('user')->getLoginAttempts($this->ip, $this->email);
        if ($attempts) {
            if ($attempts['attempts'] > 10) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a user is banned
     * 
     * If the user attempting to log in has an account and the 
     * users group level is 0. They have been banned. This method
     * is used in $this->validate();
     * 
     * @example, group level 0 = Banned
     * @see $this->validate();
     * @return bool
     */
    private function userIsBanned() 
    {
        if ($this->user['group'] == 0) {
            return true;
        }
        return false;
    }

    /**
     * Check if a users activation is pending
     * 
     * If the user attempting to log in has an account and the 
     * users group level is 1. They have been banned. This method
     * is used in $this->validate();
     *
     * @example, group level 1 = Un-activated
     * @see $this->validate();
     * @return bool
     */
    private function activationPending() 
    {
        if ($this->user['group'] == 1) {
            return true;
        }
        return false;
    }

    /**
     * Live validation
     *
     * Validate user input as they type. This method should be called via ajax.
     * 
     * @see root/public/javascript/validate.js
     * @see roo/public/html/account/login.htm
     * @return void
     */
    public function validateLive()
    {
        if (!empty($_POST['email']) && !Validate::email($_POST['email'])) {
            $this->error['email'] = Language::get('signup/email_invalid');
        }

        if (isset($this->error)) {
            Output::json(['errors' => $this->error]);
        }
    }
}