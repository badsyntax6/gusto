<?php 

/**
 * Header Controller Class
 *
 * The HeaderController handles logic specific to the header and displays the 
 * header view. The HeaderController should be loaded in each controller class 
 * where a header is desired.
 */
class HeaderController extends Controller
{
    /**
     * Index method
     *
     * 
     */
    public function index($title = null)
    {    
        if (isset(App::$urlArray[0])) $url = isset(App::$urlArray) ? App::$urlArray : ['Gusto'];
        else $url[0] = 'Gusto';

        $view['title'] = isset($title) ? $title : Language::get($url[0] . '/title');
        $view['logged'] = Auth::isLogged();
        $view['view_text'] = Language::get('header/view_text');
        $view['account_text'] = Language::get('header/account_text');
        $view['search'] = Load::view('common/search');
        $view['theme'] = '';

        if (Auth::isLogged()) {
            $view['theme'] = isset(Auth::user()->theme) && Auth::user()->theme == 1 ? 'dark-theme' : 'light-theme';
            $view['name'] = Auth::firstname() . ' ' . Auth::lastname();
            $view['avatar'] = isset(Auth::user()->avatar) ? '/uploads/account/' . Auth::user()->avatar : '/uploads/account/default_avatar.png';

            $data['last_active'] = date('c');
            $data['user_id'] = Auth::id();

            Load::model('user')->updateBy('user_id', $data);
        }

        return Load::view('common/header', $view);
    }

    public function getNotifications() 
    {
        $notifications = Load::model('user', 'alerts')->getAll();
        $output = [];

        foreach ($notifications as $key => $value) {
            $ids = explode(',', $notifications[$key]['user_anchors']);
            if (in_array(Auth::id(), $ids)) {
                if ($value['date_created']) $value['time_ago'] = getTimeAgo($value['date_created'], false);
                array_push($output, $value);
            }
            
        }

        Output::json($output);
    }
}