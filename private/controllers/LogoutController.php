<?php 

/**
 * Logout Controller Class
 *
 * The Logout controller will verify logouts and logout inactive users.
 */
class LogoutController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * @link //root/logout
     * @link //root/logout/index
     *
     * This index method uses the action parameter to decide what kind of
     * logout is to take place.
     *
     * @param string
     */
    public function index($action = null)
    {   
        $inactive = false;

        if (Auth::user()->verify_logout == 0) {
            $log = Language::get('logout/log_logged_out', ['name' => Auth::fullname()]);
            if ($this->destroySession($log)) Load::route('/dashboard');
        }

        if ($action == 'confirm') {
            $log = Language::get('logout/log_logged_out', ['name' => Auth::fullname()]);
            if ($this->destroySession($log)) Load::route('/dashboard');          
        }

        if ($action == 'inactive') {
            if (Auth::isLogged()) {
                $inactive = true; 
                $log = Language::get('logout/log_inactive_logout', ['name' => Auth::fullname()]);
                $this->destroySession($log);
            }
        }

        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['inactive'] = $inactive;

        Output::html('account/logout', $view); 
    }

    /**
     * Destroy User Session
     *
     * Destroy the session, logging the user out. After the session is destroyed redirect 
     * them to the homepage. This method wil be called by this classes index.
     */
    public function destroySession($message)
    {
        Log::event($message);
        unset($_SESSION['id']);
        session_destroy();
        return true;
    }
}