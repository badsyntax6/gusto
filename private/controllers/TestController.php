<?php 

/**
 * Test Controller
 * 
 * Have fun
 */
class TestController extends Controller
{
    public function beforeFilter()
    {
        // $mail = Load::model('settings')->getMailSettings();
        // $mail_library = Load::library('mail');
        // $mail_library->setSmtpSettings($mail);

        /* $recipients = [
            'Chase' => 'chase@techsourcehawaii.com',
            'Bad' => 'badsyntaxx@gmail.com',
            'Mr Asahina' => 'chase@yourtechconnection.com'
        ]; */

        // $mail_library->mailer->addAddress('chase@techsourcehawaii.com', 'Chaser');   
        // $mail_library->mailer->setFrom('chase@yourtechconnection.com', 'Gusto');
        // $mail_library->mailer->addReplyTo('chase@techsourcehawaii.com', 'Gusto Information');
        // $mail_library->mailer->Subject = 'Test Email';
        // $mail_library->mailer->Body = '<strong>Success!</strong> This is a test email from Gusto';
        // $mail_library->mailer->AltBody = 'Success! This is a test email from Gusto';

        // if ($mail_library->send()) {
        //     $output = ['alert' => 'success', 'message' => Language::get('settings/email_sent')];
        // } else {
        //     $output = ['alert' => 'error', 'message' => Language::get('settings/email_fail')];
        // }

        // Output::json($output);





        // echo strtotime('2019-04-3 10:13:09.000000');
        // echo 'The date "2019-04-3 10:13:09" was ' . getTimeAgo(1554322389, true). '<br><br>';

        // echo 'There are ' . countdown('December 25, 2021 12:00 am', true) . ' until "December 25, 2021 12:00 am"';

        // exit();
    }

    public function afterFilter()
    {

    }

    public function index()
    {     
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();

        Output::html('common/test', $view);
        /* $settings['table'] = 'user'; 
        $settings['orderby'] = 'user_id'; 
        $settings['direction'] = 'asc'; 
        $settings['page'] = 1; 
        $settings['limit'] = 10; 
        $settings['search_keys'] = ['firstname', 'lastname', 'email', 'group', 'last_active']; 
        $settings['search_string'] = '';
        $settings['foo'] = false; */

        /* $settings = rewriteArray($settings, $_POST);

        $users = $this->sql($settings);

        $paginated = paginate($users);

        var_dump($paginated); */
    }

    public function sql($settings)
    {
        $start = ($settings['page']-1) * $settings['limit'];

        if (isset($settings['search_string']) && !empty($settings['search_string'])) {

            $select = Load::model('user')
            ->whereLikeArray($settings['search_keys'], $settings['search_string'])
            ->orderBy($settings['orderby'], $settings['direction'])
            ->limitBetween($start, $settings['limit'])
            ->selectDistinct('*');

            $count = Load::model('user')
            ->whereLikeArray($settings['search_keys'], $settings['search_string'])
            ->orderBy($settings['orderby'], $settings['direction'])
            ->limitBetween($start, $settings['limit'])
            ->countDistinct('*');

            
            
        } else {
            $select = Load::model('user')->orderBy($settings['orderby'], $settings['direction'])->limitBetween($start, $settings['limit'])->select('*');
            $count = Load::model('user')->count();
        }

        $total = $count['response'];

        var_dump($total);

        if ($select && $select['status'] == 'success') {
            return empty($select['response']) ? [] : ['settings' => $settings, 'records' => $select['response'], 'total' => (int) $total, 'start' => $start];
        } else {
            return false;
        }
    }

    public function getUserJson() 
    {
        $user = Load::model('user');

        $locked = $user->countWhere('group', 0);
        $pending = $user->countWhere('group', 1);
        $registered = $user->countWhere('group', 2);
        $mods = $user->countWhere('group', 3);
        $admins = $user->countWhere('group', 4);

        $data['locked'] = $locked ? (int)$locked : 0;
        $data['pending'] = $pending ? (int)$pending : 0;
        $data['registered'] = $registered ? (int)$registered : 0;
        $data['mods'] = $mods ? (int)$mods : 0;
        $data['admins'] = $admins ? (int)$admins : 0;

        Output::json($data);
    }

    public function checkDay()
    {
        $timestamp = new DateTime('2018-12-27');
        $today = new DateTime(); // This object represents current date/time

        $timestamp->setTime(0, 0, 0); // reset time part, to prevent partial comparison
        $today->setTime(0, 0, 0); // reset time part, to prevent partial comparison

        $diff = $today->diff($timestamp);
        $diff_days = (int)$diff->format('%R%a'); // Extract days count in interval

        switch($diff_days) {
            case 0:
                echo '//Today';
                break;
            case -1:
                echo '//Yesterday';
                break;
            case +1:
                echo '//Tomorrow';
                break;
            default:
                echo '//Sometime';
        }
    }

    public function validate()
    {
        $email = 'chase@gusto.com';
        $tel = '12345678901234567';
        $num = '45';
        $url = 'http://foobar';
        $words = 'Hello World';
        $alpha = 'Foo';
        $alpha_num = '123foo456';
        $text = 'Whatever is @!? foo bar baz %$';
        $custom = 'abc'; 
        
        if (Validate::num($num)) {
            echo 'SUCCESS! ' . $num . ' is valid.';
        } else {
            echo 'ERROR! ' . $num . ' is invalid.';
        }

        echo '<pre>';
        var_dump(Validate::min(8)->max(20)->required()->email($email)); // Email valid and minimum 8 chars and maximum 20 chars
        var_dump(Validate::tel($tel));
        var_dump(Validate::num($num));
        var_dump(Validate::url($url));
        var_dump(Validate::words($words));
        var_dump(Validate::alpha($alpha));
        var_dump(Validate::alphaNum($alpha_num));
        var_dump(Validate::text($text));
        var_dump(Validate::custom($custom, '^[a-zA-Z]{2,4}$')); // Custom regex patter ^[a-zA-Z0-9]{1,4}$ will match string with alphabet and numbers between 2 and 4 chars long
        echo '</pre>';
    }

    public function testInput() {
        var_dump($_POST);
    }
}