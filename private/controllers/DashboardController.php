<?php 

/**
 * Dashboard Controller Class
 */
class DashboardController extends Controller
{
    /**
     * Init method
     * 
     * @link //root/dashboard
     * @link //root/dashboard/index
     *
     * @return void
     */
    public function index()
    {   
        $users = $this->getUserData();
        $settings = $this->getSettingsData();
        $logs = Load::model('log')->getLogs();
        $errors = Load::model('log')->getErrors();

        foreach ($users as $key => $value) $view[$key] = $value;
        foreach ($settings as $key => $value) $view[$key] = $value;

        $view['logs'] = $this->formatLogs($logs);
        $view['errors'] = $this->formatLogs($errors);
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['user_group'] = Auth::group();
        $view['pagination'] = Load::view('common/pagination');
        $view['controls'] = Load::view('pagination/controls', ['group' => Auth::group()]);
        $view['url'] = '/pagination/getTable';

        Output::html('common/dashboard', $view);
    }

    /** 
     * Format logs for readability
     *
     * @return array
     */
    private function formatLogs($logs = [])
    {
        if (!empty($logs)) {
            foreach ($logs as &$log) {
                $log['time'] = date('d/m/Y h:ia', strtotime($log['time']));
            }
        }
        return $logs;
    }

    /**
     * Get user data
     *
     * @return array
     */
    private function getUserData()
    {
        $users = Load::model('user');
        $total_users = $users->countAll();
        $locked = $users->countWhere('group', 0);
        $pending = $users->countWhere('group', 1);
        $registered = $users->countWhere('group', 2);
        $mods = $users->countWhere('group', 3);
        $admins = $users->countWhere('group', 4);

        $data['total_users'] = $total_users ? $total_users : 0;
        $data['locked'] = $locked ? $locked : 0;
        $data['pending'] = $pending ? $pending : 0;
        $data['registered'] = $registered ? $registered : 0;
        $data['mods'] = $mods ? $mods : 0;
        $data['admins'] = $admins ? $admins : 0;

        return $data;
    }

    /**
     * Get settings data
     *
     * @return array
     */
    private function getSettingsData()
    {
        $model = Load::model('settings');
        $settings = $model->getSettings('language');

        foreach ($settings as $s) {
            switch ($s['name']) {
                case 'owners_email': $data[$s['name']] = $s['value']; break;
                case 'strong_pw': $data[$s['name']] = isset($s['value']) ? $s['value'] : 'Off' ; break;
                case 'inactivity_limit': $data[$s['name']] = $s['value']; break;
                case 'language': $data[$s['name']] = ucfirst($s['value']); break;
                case 'owners_email': $data[$s['name']] = $s['value']; break;
                case 'maintenance_mode': $data[$s['name']] = isset($s['value']) ? 'On' : 'Off' ; break;
                default: $data[$s['name']] = $s['value']; break;
            }
        }

        return $data;
    }

    /**
     * Clear log
     * 
     * This method truncates the log table in the database
     *
     * @return void
     */
    public function clearLog()
    {
        if (Load::model('log')->clearLog()) {
            $output['alert'] = 'success';
            $output['message'] = Language::get('settings/logs_cleared');
            Output::json($output);
        }
    }

    /**
     * Clear errors
     * 
     * This method truncates the error table in the database
     *
     * @return void
     */
    public function clearErrors()
    {
        if (Load::model('log')->clearErrors()) {
            $output['alert'] = 'success';
            $output['message'] = Language::get('settings/errors_cleared');
            Output::json($output);
        }
    }
}