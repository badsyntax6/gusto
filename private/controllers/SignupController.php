<?php 

/**
 * Signup Controller Class
 *
 * This class handles user sign up. It saves post data from the signup form 
 * to the database and also sends the initial activation email.
 */
class SignupController extends Controller
{   
    /**
     * Username from signup form.
     * @property string
     */
    private $username;

    /**
     * Firstname from signup form.
     * @property string
     */
    private $firstname;

    /**
     * Lastname from signup form.
     * @property string
     */
    private $lastname;

    /**
     * Email from signup form.
     * @property string
     */
    private $email;

    /**
     * Error list
     * @property object
     */
    private $error;

    /**
     * Index method
     * 
     * @link //gusto/signup
     * @link //gusto/signup/index
     * 
     * @return void
     */
    public function index()
    {      
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();

        Output::html('account/signup', $view);
    }

    /**
     * Validate the signup form
     * 
     * This is the main validation method and will call all the methods necessary for the 
     * signup to complete. This method will be called via an AJAX function which can be 
     * found in the signup.htm view.
     */
    public function validate()
    {   
        botTest($_POST['red_herring']);

        $this->validateFirstname();
        $this->validateLastname();
        $this->validateEmail();
        $this->validatePhone();
        $this->validatePassword();
        $this->registerUser();
        $this->sendActivationEmail();
    }

    /**
     * Validate firstname
     *
     * This method will ensure that the firstname entered by the user is valid
     * This method should be called from the validate() method of this controller.
     * 
     * @see $this->validate();
     */
    private function validateFirstname() 
    {   
        if (Validate::required()->alpha($_POST['firstname'])) {
            $this->firstname = trim($_POST['firstname']);
        } else {
            $this->error['firstname'] = Language::get('signup/firstname_invalid');
        }
    }

    /**
     * Validate lastname
     *
     * This method will ensure that the lastname entered by the user is valid. 
     * This method should be called from the validate() method of this controller.
     * 
     * @see $this->validate();
     */
    private function validateLastname() 
    {   
        if (Validate::required()->alpha($_POST['lastname'])) {
            $this->lastname = trim($_POST['lastname']);
        } else {
            $this->error['lastname'] = Language::get('signup/lastname_invalid');
        }
    }

    /**
     * Validate user email
     * 
     * This method will ensure that the email entered by the user is valid. 
     * It also checks if the email is already taken. This method should be 
     * called from the validate() method of this controller
     * 
     * @see $this->validate();
     */
    private function validateEmail()
    {
        $this->email = trim(strtolower($_POST['email']));
        if (Validate::required()->email($this->email)) {
            $this->email = $this->email;
        } else {
            $this->error['email'] = Language::get('signup/email_invalid');
        }

        if (Load::model('user')->getWhere('email', $this->email)) {
            $this->error['email'] = Language::get('signup/email_taken');
        }
    }

    private function validatePhone()
    {
        if (!empty($_POST['phone'])) {
            if (Validate::tel($_POST['phone'])) {
                $this->phone = trim($_POST['phone']);
            } else {
                $this->error['phone'] = Language::get('signup/phone_invalid');
            }
        }
    }

    /**
     * Validate user password
     * 
     * This method is used to check if the passwords entered match. If the strong password setting is on, 
     * then it will also check if the password is too weak. Then it will hash the users password and 
     * store it in a property. This method should be called from the validate() method of this controller.
     * 
     * @see $this->validate();
     */
    private function validatePassword()
    {
        if (Load::model('settings')->getSetting('strong_pw')) {
            if (!Validate::required()->password($_POST['password'])) {
                $this->error['password'] = Language::get('signup/password_weak');
            }
        }

        if ($_POST['password'] !== $_POST['confirm']) {
            $this->error['confirm'] = Language::get('signup/password_match');
        } 

        $this->password = password_hash($_POST['password'], PASSWORD_BCRYPT, array('cost' => 12));
    }

    /**
     * Register a new user
     * 
     * This method will insert a user into the database using properties created in this controller. 
     * If there are errors the script will exit so a user cannot be created. This method should be 
     * called from the validate() method of this controller.
     * 
     * @see $this->validate();
     */
    private function registerUser()
    {   
        if (isset($this->error)) {
            $errors = '';
            
            foreach ($this->error as $e) {
                $errors .= $e . ' ';
            }

            Log::event(Language::get('signup/log_signup_attempt', ['email' => $this->email, 'errors' => $errors]));
            exit(Output::json(['errors' => $this->error]));
        }

        $data['username'] = '';
        $data['firstname'] = $this->firstname;
        $data['lastname'] = $this->lastname;
        $data['email'] = $this->email;
        $data['password'] = $this->password;
        $data['key'] = md5(mt_rand());
        $data['group'] = 1;
        $data['signup_date'] = date('c');
        $data['ip'] = $_SERVER['REMOTE_ADDR'];

        if (Load::model('user')->save($data)) {

            $user = Load::model('user')->getWhere('email', $this->email);
            $this->user_id = $user['user_id'];

            if ($user['user_id'] == 1) {
                $data['user_id'] = $user['user_id'];
                $data['group'] = 4;
                Load::model('user')->updateBy('user_id', $data);
                Load::model('settings')->updateBy('setting_id', ['value' => $this->email, 'setting_id' => $user['user_id']]);
            }

            Load::model('user')->createUserMenuSetting($user['user_id']);
        } else {
            Output::json(['alert' => 'error', 'message' => Language::get('signup/signup_fail')]);
        }
    }

    /**
     * Send activation email
     *
     * This method accesses the mail library and sends activation emails to clients that sign up. 
     * This method should be called from the validate() method of this controller.
     * 
     * @see $this->validate();
     */
    private function sendActivationEmail()
    {
        $settings = Load::model('settings');
        $mail = $settings->getMailSettings();
        $mail_library = Load::library('mail');
        $mail_library->setSmtpSettings($mail);
        $link = HOST . '/users';

        $mail_library->mailer->addAddress($settings->getSetting('owners_email')); 
        $mail_library->mailer->setFrom('chase@yourtechconnection.com', 'Gusto');
        $mail_library->mailer->Subject = 'New User';
        $mail_library->mailer->Body = str_replace('{{link}}', $link, App::getTemplate('email/new_user'));
        $mail_library->mailer->AltBody = "A new user signed up. {$link}";

        if ($mail_library->send()) {
            $link = HOST . '/login';

            $mail_library->mailer->addAddress($this->email); 
            $mail_library->mailer->setFrom('chase@yourtechconnection.com', 'Gusto');
            $mail_library->mailer->Subject = 'Welcome';
            $mail_library->mailer->Body = str_replace('{{link}}', $link, App::getTemplate('email/welcome'));
            $mail_library->mailer->AltBody = 'An administrator will activate your account soon.';

            if ($mail_library->send()) {
                $output = ['alert' => 'success', 'message' => Language::get('signup/signup_success')];
            }
        } else {
            $output = ['alert' => 'error', 'message' => Language::get('signup/activate_mail_fail')];
        }

        Log::event(Language::get('signup/log_new_user', ['name' => $this->firstname . ' ' . $this->lastname]));
        Output::json($output);
    }

    /**
     * Live validation
     *
     * Validate user input as they type. This method should be called via ajax.
     * 
     * @see root/public/javascript/validate.js
     * @see roo/public/html/account/signup.htm
     * @return void
     */
    public function validateLive()
    {
        if (!empty($_POST['firstname']) && !Validate::alpha($_POST['firstname'])) {
            $this->error['firstname'] = Language::get('signup/firstname_invalid');
        }
        if (!empty($_POST['lastname']) && !Validate::alpha($_POST['lastname'])) {
            $this->error['lastname'] = Language::get('signup/lastname_invalid');
        }
        if (!empty($_POST['email']) && !Validate::email($_POST['email'])) {
            $this->error['email'] = Language::get('signup/email_invalid');
        }

        $this->checkEmailTaken();

        if (!empty($_POST['password']) && !Validate::password($_POST['password'])) {
            if (Load::model('settings')->getSetting('strong_pw')) {
                $this->error['password'] = Language::get('signup/password_weak');
            }
        }
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            if ($_POST['password'] !== $_POST['confirm']) {
                $this->error['confirm'] = Language::get('signup/password_match');
            }
        } 

        if (isset($this->error)) {
            Output::json(['errors' => $this->error]);
        }
    }

    /**
     * Check if a username is taken already
     * 
     * Validate user input as they type. This method should be called via ajax.
     *
     * @return void
     */
    public function checkUsernameTaken()
    {
        if (!empty($_POST['username'])) {
            $username = trim(strtolower($_POST['username']));
            if ($user = Load::model('user')->getWhere('username', $username)) {
                if ($username == trim(strtolower($user['username']))) {
                    $this->error['username'] = Language::get('signup/username_taken');
                }
            }
        }
    }

    /**
     * Check if an email is taken already
     *
     * Validate user input as they type. This method should be called via ajax.
     * 
     * @return void
     */
    public function checkEmailTaken()
    {
        if (!empty($_POST['email'])) {
            $email = trim(strtolower($_POST['email']));
            if ($user = Load::model('user')->getWhere('email', $email)) {
                if ($email == trim(strtolower($user['email']))) {
                    $this->error['email'] = Language::get('signup/email_taken');
                }
            }
        }
    }
}