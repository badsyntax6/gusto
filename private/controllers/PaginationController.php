<?php 

/**
 * Pagination Controller Class
 *
 * This class gets Pagination information and has the ability to alter a Pagination status.
 */
class PaginationController extends Controller
{
    /**
     * Index method
     *
     * Routes
     * @link //root/pagination
     * @link //root/pagination/index
     */
    public function index()
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['pagination'] = Load::view('common/pagination');
        $view['controls'] = Load::view('pagination/controls', ['group' => Auth::group()]);

        Output::html('pagination/view', $view);
    }

    /**
     * Get paginated records for a table
     * 
     * Get user records, paginate them, and output them as json. This method is tightly
     * coupled with the pagination javascript file.
     * 
     * @see Pagination /root/public/javascript/pagination.js
     * @see PaginationModel /root/private/models/PaginationModel.php
     * 
     * @return void
     */
    public function getTable()
    {
        $settings['table'] = 'user'; 
        $settings['orderby'] = 'user_id'; 
        $settings['direction'] = 'asc'; 
        $settings['page'] = 1;
        $settings['limit'] = 10; 
        $settings['search_keys'] = ['firstname', 'lastname', 'email', 'group', 'last_active']; 
        $settings['search_string'] = '';

        $settings = rewriteArray($settings, $_POST);
        $users = Load::model('user')->getUsersForPagination($settings);
        $paginated = paginate($users);

        $output['list'] = Load::view('pagination/table', $paginated);
        $output['table'] = $settings['table'];
        $output['orderby'] = $settings['orderby'];
        $output['direction'] = $settings['direction'];
        $output['limit'] = $settings['limit'];
        $output['page'] = $settings['page'];
        $output['start'] = $paginated['start'];
        $output['total_pages'] = $paginated['pages'];
        $output['total_records'] = $paginated['total'];
        $output['search_keys'] = $settings['search_keys'];
        $output['search_string'] = $settings['search_string'];
        $output['query'] = $paginated['query'];

        Output::json($output);
    }
}