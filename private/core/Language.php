<?php 

/**
 * Language Core Class
 * 
 * This language library will be loaded in the application controller. So you can 
 * always use the language library in your controller classes.
 * @example, Language::get('filename/key');
 */
class Language
{
    /**
     * Get language setting
     * 
     * Get the language setting from the settings table and return it.
     *
     * @return string 
     */
    public static function getLanguageSetting()
    {
        $query = Database::instance()->mysqli->query('SELECT `value` FROM `settings` WHERE `name` = "language"');
        $lang = $query->fetch_object();  
        return $lang->value;
    }

    /**
     * Return language value
     * 
     * Includes a language file and returns values from it. Language files have placeholders in them that 
     * look like so {{name}}. The $vars parameter contains keys with matching names to replace the
     * placeholders with new values.
     * 
     * @param string $path The path to the language file and index.
     * @param array $vars Words to replace placeholders in language files
     * 
     * @return string
     * 
     * @see /root/private/language/
     * 
     * @example, Language::get('home/title');
     */
    public static function get($path, $vars = null)
    {
        $language = self::getLanguageSetting();
        $path = explode('/', $path);
        $file = isset($path[0]) ? $path[0] : 'home';
        $index = isset($path[1]) ? $path[1] : '';

        if (is_file(LANGUAGE_DIR . '/' . $language . '/' . $file . '.php')) {
            include LANGUAGE_DIR . '/' . $language . '/' . $file . '.php';
        } else {
            include LANGUAGE_DIR . '/' . $language . '/dashboard.php';
        }

        if (isset($_[$index])) {
            $result = isset($vars) ? self::replaceVarPlaceholders($_[$index], $vars) : $_[$index];
            return $result;
        } else {
            return 'Error: Language key (' . $index . ') not found in (' . $file . ').';
        }
    }

    /**
     * Replace placeholders in language strings
     * 
     * Replace the placeholders in the language string with their counterparts in the vars array.
     *
     * @param string $string
     * @param array $vars Variables to replace placeholders
     * 
     * @return string
     */
    public static function replaceVarPlaceholders($string, $vars)
    {
        foreach ($vars as $key => $value) {
            $string = str_replace('{{' . $key . '}}', $value, $string);
        }

        return $string;
    }
}