<?php 

/**
 * Router class
 * 
 * The Router class will examine the url, and compare it to routes in the routing
 * array. If a route is found, the user will be dispatched to the route.
 */
class Router
{
    /**
     * Controller class
     * @property object
     */
    private static $controller = 'DashboardController';

    /**
     * Controller method
     * @property string
     */
    private static $action = 'index';

    /**
     * Controller parameters
     * @property array
     */
    private static $params = [];

    /**
     * Route access level
     * @property int
     */
    protected static $route_level;

    /**
     * Sort through the routes
     * 
     * Sort through each route from the routes array. The routes data is then passed to the dispatch 
     * method as parameters when a match is found. If no match is found, the dispatch method is still 
     * called but with the 404 set of perameters.
     * 
     * @param array $routes
     * @see /root/private/routes.php
     * @return void
     */
    public static function sort($routes)
    {
        foreach ($routes as $r) {     
            $url = isset($_GET['url']) ? filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL) : '/';
            $route = preg_replace("/\//", "\\/", $r['route']);
            $route = preg_replace('/\{([^\}]+)\}/', '(\1)', $route);

            if (preg_match("/^$route$/", $url, $matches)) {
                array_shift($matches);
                $params = isset($matches) ? $matches : [];
                self::$route_level = $r['level'];
                return self::dispatch($r['controller'], $r['action'], $params);
            }
        } 

        return self::dispatch('StatusController', 'index', ['404']);
    }

    /**
     * Distpach the user to the route
     * 
     * Using the controller, instantiate the controller class, and using 
     * the action string, call the action method. $params will be used 
     * as parameters for the action/method. 
     * @example, $controller->$action($params)
     * 
     * @see self::init()
     * @param string $controller - Controller class name
     * @param string $action - Controller method name
     * @param array $params - Parameters for the controller method
     * @return void
     */
    private static function dispatch($controller, $action, $params = [])
    {
        if (class_exists($controller)) self::$controller = new $controller();
        else $controller = 'StatusController'; 

        if (method_exists(self::$controller, $action)) self::$action = $action;
        else array_unshift($params, $action);

        self::$params = array_values($params);
        
        if (is_callable([self::$controller, self::$action])) {
            if (method_exists(self::$controller, 'beforeFilter')) self::$controller->beforeFilter();
            call_user_func_array([self::$controller, self::$action], self::$params);
            if (method_exists(self::$controller, 'afterFilter')) self::$controller->afterFilter();
        } else {
            exit('Could not find index');
        }

        // echo Model::$db_hits;
    }
}
