<?php 

/**
 * Model Core Class
 *
 * The model class is the main model of the application system. All model classes will be extensions of this class.
 * The model class included query templating and some generic queries to reduce redundant model code.
 * 
 * @version 2020.04.16.11.48,
 * @author Chase Asahina <chase@techsourcehawaii.com>
 * @license Your mom
 * 
 * @example, SELECTS
 *           - $this->table('user')->select('*'); == SELECT * FROM `user`
 *           - $this->table('user')->where('user_id, 1)->select('firstname'); == SELECT `firstname` FROM `user` WHERE `user_id` = 1
 *           -  $this->table('user')->where('user_id', 1)->orWhere('firstname', 'Bilbo')->andWhere('lastname', Baggins)->select('firstname, lastname'); == SELECT `firstname`, `lastname` FROM `user` WHERE `user_id` = 1 OR `firstname` = "Chase" AND `lastname` = "Asahina"
 *
 * @example, INSERT
 *           - $this->table('user')->insert($data); == INSERT INTO `user`(`firstname`, `lastname`) VALUES (?, ?)
 * 
 * 
 * @example, UPDATE 
 *           - $data = ['firstname' => 'Grima', 'lastname' => 'Wormtongue', 'user_id' => 3];
 *           - $this->table('user')->where('user_id')->update($data); == UPDATE `user` SET `firstname`= ?, `lastname` = ? WHERE `user_id` = ?
 *           - $data = ['firstname' => 'Samwise', 'lastname' => 'Gamgee', 'group' => 'admin', 'user_id' => 3];
 *           - $this->table('user')->where('user_id')->andWhere(group)->update($data); == UPDATE `user` SET `firstname` = ?, `lastname` = ? WHERE `user_id` = ? AND `group` = ?
 */
class Model
{
    /**
     * Database instance
     * @property object
     */
    private $con;

    /**
     * Table to be used in the query
     * @property string
     */
    public $table;

    /**
     * The query string
     * @property string
     */
    private $query = '';

    /**
     * Post data to be inserted or updated
     * @property mixed
     */
    private $post_data;

    /**
     * Limit of records to get
     * @property int
     */
    private $limit;

    public static $db_hits = 0;

    /**
     * Model construct
     * 
     * Set the table name property and database instance property. It is intended that table names
     * and the first part of model names match up. So by default when loading a model, the model name
     * will be used as the table name for the query. For example when loading a model `Load::model('user')`, 
     * `user` will be the table name used in the query string. This can be overridden when loading a model by 
     * passing a parameter to the Load model method. For example Load::model('user', 'menus'). This
     * will load the `user` model but interact with the menus table. It's also possible to override the table
     * by passing the table name as a string to the table method in an extended model class.
     * 
     * @param string $table Table name to be used in query string.
     * 
     * @return void
     * 
     * @see /root/private/core/Load.php
     */
    public function __construct($table)
    {
        $this->table = $table;
        $this->con = Database::instance();
    }

    /**
     * Table
     * 
     * Begin building the query string by creating a table property. Also reset the query properties 
     * back to null so we can start a fresh query.
     *
     * @param string $table Table to be used in the query.
     * 
     * @return self
     */
    public function table($table)
    {
        $this->post_data = null;
        $this->query = null;
        $this->limit = null;
        $this->table = $table;    

        return $this;
    }

    public function insertResponse($insert)
    {
        if ($insert['status'] == 'success') return empty($insert['response']) ? true : $insert['response'];
        else return false;
    }

    public function selectResponse($select)
    {
        if ($select && $select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
        else return false;
    }

    public function updateResponse($update, $change_required = true)
    {
        if ($change_required) {
            if ($update && $update['status'] == 'success' && $update['affected_rows'] > 0) return empty($update['response']) ? true : $update['response'];
            else return false;
        }
        
        if ($update && $update['status'] == 'success') return empty($update['response']) ? true : $update['response'];
        else return false;
    }

    /**
     * Select
     * 
     * Build the select portion of the query string.
     *
     * @param string $select Database values to retrieve.
     * 
     * @example, $this->table('user')->select('*');
     * @example, $this->table('user')->select('firstname, lastname');
     * 
     * @return object
     */
    public function select($select)
    {
        if (strpos($select, ',')) $choices = explode(', ', $select);
        else $choices = [$select];

        foreach ($choices as $c) $selects[] = $select == '*' ? $c : '`' . $c .  '`';

        $selects = implode(', ', $selects);

        $this->query = 'SELECT ' . $selects . ' FROM `' . $this->table . '`' . $this->query;

        return $this->fetch();
    }

    /**
     * Select Distinct
     * 
     * Build the select portion of the query string. Selecting distinct records removes duplicates
     * from the result
     *
     * @param string $select Database values to retrieve.
     * 
     * @example, $this->table('user')->selectDistinct('*');
     * @example, $this->table('user')->selectDistinct('firstname, lastname');
     * 
     * @return object
     */
    public function selectDistinct($select) 
    {
        if (strpos($select, ',')) $choices = explode(', ', $select);
        else $choices = [$select];

        foreach ($choices as $c) $selects[] = $select == '*' ? $c : '`' . $c .  '`';

        $selects = implode(', ', $selects);

        $this->query = 'SELECT DISTINCT ' . $selects . ' FROM `' . $this->table . '`' . $this->query;

        return $this->fetch();
    }

    /**
     * Count records in a table
     *
     * @return object
     */
    public function count()
    {
        $this->query = 'SELECT COUNT(1) FROM `' . $this->table . '`' . $this->query;
        return $this->fetch();
    }

    /**
     * Count distinct records in a table
     * 
     * Counting distinct records removes duplicates from the result
     *
     * @return object
     */
    public function countDistinct()
    {
        $this->query = 'SELECT DISTINCT COUNT(1) FROM `' . $this->table . '`' . $this->query;
        return $this->fetch();
    }

    /**
     * Count records in a table where like
     *
     * @return object
     */
    public function countWhereLike($key, $value)
    {
        //SELECT count(case when firstname like '%ba%' then 1 end) FROM user
        $this->query = 'SELECT COUNT(CASE WHEN `' . $key . '` LIKE "%'. $value . '%" THEN 1 END) FROM `' . $this->table . '`';
        return $this->fetch();
    }

    /**
     * Build the WHERE portion of the query
     *
     * Create the where portion of the string. If the value parameter is not set the value becomes ?.
     * The question mark value is for updates because this class does mysqli prepare and bind param.
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('user_id', 5)->select('*');
     * 
     * @example, - $data = ['firstname' => 'Frodo', 'user_id' => 5];
     *           - $this->table('user')->where('user_id')->update($data);
     */
    public function where($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' WHERE `' . $key . '` = ?';
        else $this->query .= ' WHERE `' . $key . '` = "' . $value . '"';        
        return $this;
    }

    /**
     * Build the AND portion of the query
     * 
     * If the value parameter is not set the value becomes ?.
     *
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('firstname', 'Bilbo')->andWhere('lastname', 'Baggins')->select('*');
     * 
     * @example, - $data = ['firstname' => 'Grima', 'lastname' => 'Wormtongue', 'email' => 'newemail@gmail.com'];
     *           - $this->table('user')->where('firstname')->andWhere('lastname')->update($data);
     */
    public function andWhere($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' AND `' . $key . '` = ?';
        else $this->query .= ' AND `' . $key . '` = "' . $value . '"';        
        return $this;
    }

    /**
     * Build the OR portion of the query
     * 
     * If the value parameter is not set the value becomes ?.
     *
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('firstname', 'Bilbo')->orWhere('user_id', 1)->select('*');
     * 
     * @example, - $data = ['firstname' => 'Bilbo', 'user_id' => 1, 'email' => 'newemail@gmail.com'];
     *           - $this->table('user')->where('firstname')->orWhere('user_id')->update($data);
     */
    public function orWhere($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' OR `' . $key . '` = ?';
        else $this->query .= ' OR `' . $key . '` = "' . $value . '"';
        return $this;
    }

    /**
     * Build a WHERE NOT portion of the query
     *
     * If the value parameter is not set the value becomes ?.
     * 
     * @param string $key Database key / column name to exclude the query to.
     * @param mixed $value Value to exclude from the query.
     * 
     * @return self
     *             
     * @example, - $this->table('user')->whereNot('firstname', 'Bilbo')->select('*');                 
     * 
     * @example, - $data = ['firstname' => 'Bilbo', 'email' => 'newemail@gmail.com'];
     *           - $this->table('user')->whereNot('firstname')->update($data);
     */
    public function whereNot($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' WHERE `' . $key . '` != ?';
        else $this->query .= ' WHERE `' . $key . '` != "' . $value . '"';
        return $this;
    }

    /**
     * Build an AND WHERE portion of the query.
     * 
     * If the value parameter is not set the value becomes ?.
     *
     * @param string $key Database key / column name to exclude the query to.
     * @param mixed $value Value to exclude from the query.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('lastname', 'Baggins')->andWhereNot('firstname', 'Bilbo')->select('*');
     * 
     * @example, - $data = ['firstname' => 'Bilbo', 'email' => 'newemail@gmail.com'];
     *           - $this->table('user')->whereNot('firstname')->update($data);
     */
    public function andWhereNot($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' AND `' . $key . '` != ?';
        else $this->query .= ' AND `' . $key . '` != "' . $value . '"';        
        return $this;
    }

    /**
     * Build the WHERE LIKE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereLike('lastname', 'Baggins')->select('*');
     */
    public function whereLike($key, $value)
    {
        $this->query .= ' WHERE `' . $key . '` LIKE "%'. $value . '%"';
        return $this;
    }

    /**
     * Build the WHERE LIKE portion of the query
     * 
     * @param array $arr Array of database keys / column names to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereLike('lastname', 'Baggins')->select('*');
     */
    public function whereLikeArray($arr, $value)
    {
        $this->query .= ' WHERE `' . $arr[0] . '` LIKE "%'. $value . '%"';
        unset($arr[0]);
        foreach ($arr as $a) {
            $this->query .= ' OR `' . $a . '` LIKE "%'. $value . '%"';
        }
        return $this;
    }

    /**
     * Build an OR portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereLike('lastname', 'Baggins')->orWhereLike('firstname', 'Frodo')->select('*');
     */
    public function orWhereLike($key, $value)
    {
        $this->query .= ' OR `' . $key . '` LIKE "%'. $value . '%"';
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereGreaterThan('user_id' 1)->select('*');
     * 
     * @example, - $data = ['status' => 'active', 'user_id' => 1];
     *           - $this->table('user')->whereGreaterThan('user_id')->update($data);
     */
    public function whereGreaterThan($key, $value = null)
    {
        if (is_null($value)) $this->query .= ' WHERE `' . $key . '` > ?';
        else $this->query .= ' WHERE `' . $key . '` > "' . $value . '"';        
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereLesserThan('user_id' 2)->select('*');
     * 
     * @example, - $data = ['status' => 'active', 'user_id' => 2];
     *           - $this->table('user')->whereLesserThan('user_id')->update($data);
     */
    public function whereLesserThan($key, $value)
    {
        if (is_null($value)) $this->query .= ' WHERE `' . $key . '` > ?';
        else $this->query .= ' WHERE `' . $key . '` > "' . $value . '"';        
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->highest('user_id')->select('*');
     */
    public function highest($key)
    {
        $this->query .= ' WHERE `' . $key . '` = (SELECT max(' . $key . ') FROM `' . $this->table . '`)';
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param string $date Date to pull the day from.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereDay('birthday', 1987-07-16)->select('*');
     */
    public function whereDay($key, $date)
    {
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $this->query .= ' WHERE DAY(`' . $key . '`) = ' . $day . ' AND MONTH(`' . $key . '`) = ' . $month . ' AND YEAR(`' . $key . '`) = ' . $year;
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param string $date Date to pull the month from.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereMonth('birthday', 1987-07-16)->select('*');
     */
    public function whereMonth($key, $date)
    {
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $this->query .= ' WHERE MONTH(`' . $key . '`) = ' . $month . ' AND YEAR(`' . $key . '`) = ' . $year;
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param string $date Date to pull the year from.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereYear('birthday', 1987-07-16)->select('*');
     */
    public function whereYear($key, $date)
    {
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $this->query .= ' WHERE MONTH(`' . $key . '`) = ' . $month . ' AND YEAR(`' . $key . '`) = ' . $year;
        return $this;
    }

    /**
     * Build the WHERE portion of the query
     *
     * Begin building the where date portion of the query.
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param string $date Date to limit the record to.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->whereYear('birthday', 1987-07-16)->select('*');
     */
    public function whereDate($key, $date)
    {
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $this->query .= ' WHERE DAY(`' . $key . '`) = ' . $day . ' AND MONTH(`' . $key . '`) = ' . $month . ' AND YEAR(`' . $key . '`) = ' . $year;
        return $this;
    }

    /**
     * Within X amount of time
     * 
     * Build a where portion of the query that will limit the query to records where the 
     * values are within x amount of time.
     *
     * @param mixed $amount Int or string with the amount of time.
     * @param string $time Interval of time. Expects MINUTE, HOUR, DAY;
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('signup_date')->withing(2, 'days')->select('*');
     */
    public function within($amount, $time)
    {
        $time = strtoupper(rtrim($time, 's'));
        $this->query = str_replace('= ?', '', $this->query);
        $this->query .= ' > NOW() - INTERVAL ' . $amount . ' ' . $time;
        return $this;
    }

    /**
     * Older than X amount of time
     * 
     * Build a where portion of the query that will limit the query to records where the 
     * values are older than x amount of time. 
     *
     * @param mixed $amount Int or string with the amount of time.
     * @param string $time Interval of time. Expects MINUTE, HOUR, DAY;
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('signup_date')->olderThan(3, 'hours')->select('*');
     */
    public function olderThan($amount, $time)
    {
        $time = strtoupper(rtrim($time, 's'));
        $this->query = str_replace('= ?', '', $this->query);
        $this->query .= ' < NOW() - INTERVAL ' . $amount . ' ' . $time;
        return $this;
    }
    
    /**
     * Order by
     * 
     * Continue building the query string deciding which order the records will come in.
     *
     * @param string $orderby
     * @param string $direction
     * 
     * @return self
     * 
     * @example, - $this->table('user')->orderBy('lastname', 'asc')->select('lastname');
     */
    public function orderBy($orderby, $direction)
    {
        $this->query .= ' ORDER BY `' . $orderby . '` ' . strtoupper($direction);
        return $this;
    }

    /**
     * Limit
     * 
     * Continue building the query string and limiting the amount of records.
     * If limit is set to one and only a single key is selected a string will 
     * be returned.
     *
     * @param int $limit
     * 
     * @return self
     * 
     * @example, - $this->table('user')->limit(1)->select('*'); - returns an array of one users data
     * @example, - $this->table('user')->limit(1)->select('firstname'); - returns a string of one users firstname
     */
    public function limit($limit)
    {
        $this->query .= ' LIMIT ' . $limit;
        $this->limit = $limit;
        return $this;
    }

    /**
     * Limit between
     * 
     * Continue building the query string and limiting the amount
     * of records.
     *
     * @param int $start
     * @param int $end
     * 
     * @return self
     * 
     * @example, - $this->table('user')->where('group', 2)->limitBetween(4, 6)->select('*'));
     */
    public function limitBetween($start, $end)
    {
        $this->query .= ' LIMIT ' . $start . ', ' . $end;
        return $this;
    }

    /**
     * Left join
     * 
     * Begin a join portion of the query string, joinging 2 tables. Returns all records from the 
     * left table, and the matched records from the right table.
     *
     * @param string $that_table A table to join to the current table.
     * @param string $key1 Database key / column name from the left table.
     * @param string $key2 Database key / column name from the right table.
     * 
     * @return self
     * 
     * @example, - $this->table('user')->leftJoin('menus', 'user_id', 'menu_anchor')->limit(1)->select('*');
     */
    public function leftJoin($that_table, $key1, $key2)
    {
        $this->query .= ' LEFT JOIN `' . $that_table . '` ON ' . $this->table . '.' . $key1 . ' = ' . $that_table . '.' . $key2;
        return $this;
    }

    /**
     * Right join
     * 
     * Begin a join portion of the query string, joinging 2 tables. Returns all records from the 
     * right table, and the matched records from the left table.
     *
     * @param string $that_table A table to join to the current table.
     * @param string $key1 Database key / column name from the right table.
     * @param string $key2 Database key / column name from the left table.
     * 
     * @return self
     * 
     * @example, $this->table('user')->rightJoin('menus', 'user_id', 'menu_anchor')->limit(1)->select('*');
     */
    public function rightJoin($that_table, $key1, $key2)
    {
        $this->query .= ' RIGHT JOIN `' . $that_table . '` ON ' . $this->table . '.' . $key1 . ' = ' . $that_table . '.' . $key2;
        return $this;
    }

    /**
     * Inner join
     * 
     * Begin a join portion of the query string, joinging 2 tables. Returns records that have 
     * matching values in both tables.
     *
     * @param string $that_table A table to join to the current table.
     * @param string $key1 Database key / column name from the right table.
     * @param string $key2 Database key / column name from the left table.
     * 
     * @return self
     */
    public function innerJoin($that_table, $key1, $key2)
    {
        $this->query .= ' INNER JOIN `' . $that_table . '` ON ' . $this->table . '.' . $key1 . ' = ' . $that_table . '.' . $key2;
        return $this;
    }

    /**
     * Full join
     * 
     * Begin a join portion of the query string, joinging 2 tables.
     * Returns all records when there is a match in either left or right table.
     *
     * @param string $that_table A table to join.
     * @param string $key1 A column to compare to another.
     * @param string $key2 A column to be compared against.
     * 
     * @return self
     */
    public function fullJoin($that_table, $key1, $key2)
    {
        $this->query .= ' FULL JOIN `' . $that_table . '` ON ' . $this->table . '.' . $key1 . ' = ' . $that_table . '.' . $key2;
        return $this;
    }

    /**
     * Start a delete query
     *
     * @return self
     * 
     * @example, $this->table('user')->where('user_id', 5)->delete();
     */
    public function delete()
    {
        $this->query = 'DELETE FROM `' . $this->table . '`' . $this->query;
        return $this->execute();
    }

    /**
     * Truncate a table
     * 
     * @return self
     * 
     * $this->table('log')->truncate()
     */
    public function truncate()
    {
        return $this->con->mysqli->query('TRUNCATE TABLE `' . $this->table . '`');
    }

    /**
     * Fetch
     *
     * Using the query string, execute the fetch query and return an associative array.
     * 
     * @return array
     */
    public function fetch()
    {
        Model::$db_hits++;
        
        $output = [];

        if ($query = $this->con->mysqli->query($this->query)) {
            while ($col = $query->fetch_assoc()) $output[] = $col;

            if (isset($output[0]) && array_key_exists('COUNT(1)', $output[0])) {
                $output = implode(array_values($output[0]));
            }
            if ($this->limit == 1) {
                if (!empty($output)) $output = $output[0];
                if (is_array($output) && count($output) == 1) {
                    $output = isset($output[0]) ? $output[0] : $output;
                    if (isset($output) && count($output) == 1) $output = implode($output);           
                }
            }

            return ['status' => 'success', 'response' => $output, 'affected_rows' => $this->con->mysqli->affected_rows, 'query' => $this->query];
        }

        return $this->debug($this->con->mysqli->error);
    }

    /**
     * Insert
     * 
     * Using the query string prepare the data for insertion into the database.
     *
     * @param mixed $data Array of data to be inserted.
     * 
     * @return self
     */
    public function insert($data)
    {
        $data = $this->dumpBadColumns($data);
        foreach (array_keys($data) as $k) {
            $values[] = '`' . $k .  '`';
            $placeholders[] = '?';
        }

        $values = implode(', ', $values);
        $placeholders = implode(', ', $placeholders);

        $this->post_data = $data;
        $this->query = 'INSERT INTO `' . $this->table . '` (' . $values . ') VALUES (' . $placeholders . ')';

        return $this->execute();
    }

    /**
     * Update
     *
     * Using the query string prepare the data to be updated in the database.
     * 
     * @param mixed $data - Array of data to be updated.
     * 
     * @return self
     */
    public function update($data)
    {        
        $data = $this->dumpBadColumns($data);
        foreach (array_keys($data) as $k) {
            $values[] = '`' . $k .  '`';
        }

        $values = implode(' = ?, ', $values);

        $this->post_data = $data;
        $this->query = 'UPDATE `' . $this->table . '` SET ' . $values . ' = ?' . $this->query;

        return $this->execute();
    }

    /**
     * Dump Non Existant Columns
     *
     * @param array $data Should be an array of post data
     *
     * @return array Should be an array of post data with only columns that exist
     */
    public function dumpBadColumns($data)
    {
        // Dump columns that dont exist
        $pd = [];
        foreach ($data as $key => $value) {
            $result = $this->con->mysqli->query("SHOW COLUMNS FROM `$this->table` LIKE '$key'");
            $exists = ($result->num_rows) ? true : false;
            if ($exists) $pd[$key] = $value;
        }
        return $pd;
    }

    /**
     * Execute the query
     * 
     * After the data has been prepared by either the update or the insert
     * methods execute the query.
     *
     * @return array
     */
    public function execute()
    {
        Model::$db_hits++;

        if (isset($this->post_data)) {
            // Query params need to be in the same order as post data. It's also necessary to remove the WHERE/AND/OR params from the SET part of the query.
            if (preg_match_all('/(WHERE `\w+`|AND `\w+`|OR `\w+`)/', $this->query, $matches)) {
                foreach ($matches[0] as $m) {
                    $arr[] = preg_replace('/\w.* `(\w+)`/', '$1', $m);
                }
        
                $cons = array_unique($arr);

                foreach ($cons as $c) {   
                    if (strpos($this->query, ', `' . $c . '` = ?')) {
                        $this->query = str_replace(', `' . $c . '` = ?', '', $this->query);
                    }
                    if (strpos($this->query, '`' . $c . '` = ?,')) {
                        $this->query = str_replace('`' . $c . '` = ?,', '', $this->query);
                    }

                    // Move WHERE/AND/OR params to the end of the post data array.
                    $this->post_data += array_splice($this->post_data, array_search($c, array_keys($this->post_data)), 1); 
                }
            }

            $params = substr_count($this->query, '?');

            for ($i = 0; $i < $params; $i++) { 
                $types[] = 's';
            }

            if ($query = $this->con->mysqli->prepare($this->query)) {
                $this->query = null;
                if ($query->bind_param(implode($types), ...array_values($this->post_data))) {
                    if ($query->execute()) {
                        return ['status' => 'success', 'affected_rows' => $query->affected_rows];
                    } else {
                        return $this->debug($this->con->mysqli->error);
                    }
                } else {
                    return $this->debug($this->con->mysqli->error);
                }
            } else {
                return $this->debug($this->con->mysqli->error);
            }    
        }
        
        if ($this->con->mysqli->query($this->query)) {
            return ['status' => 'success', 'affected_rows' => $this->con->mysqli->affected_rows];
        } else {
            $this->debug($this->con->mysqli->error);
        }
    }

    /**
     * Debug
     * 
     * If there's an error in the sql query, log it and determine where it came from.
     *
     * @param string $sql_error The error returned by sql.
     * 
     * @return false
     */
    public function debug($sql_error)
    {
        $backtrace = debug_backtrace();
        $data = $backtrace[3];

        $data['file'] = explode('\\', $data['file']);
        $data['file'] = end($data['file']);

        $controller = str_replace('.php', '', $data['file']);
        $string = ' ERROR: ' . $data['class'] . '->' . $data['function'] . ' - ' . $sql_error . '. - Called by ' . $controller . ' on line ' . $data['line'] . '.';
        
        Log::db($string);

        return false;
    }

    /**
     * Insert records into database
     * 
     * @param array $data Data to save.    
     * 
     * @return mixed 
     */
    public function save($data)
    {
        if ($insert = $this->table($this->table)->insert($data)) {
            if ($insert['status'] == 'success') return empty($insert['response']) ? true : $insert['response'];
            else return false;
        }
    }

    /**
     * Get all records from a table
     * 
     * @return array
     */
    public function getAll()
    {
        if ($select = $this->table($this->table)->select('*')) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }

    /**
     * Get all records that match the where clause
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Values to limit the query to.
     * 
     * @return array
     */
    public function getAllWhere($key, $value)
    {
        if ($select = $this->table($this->table)->where($key, $value)->select('*')) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }

    /**
     * Get a single record row
     *
     * Get a singe record and return it in an array.
     * 
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Values to limit the query to.
     * 
     * @return array
     */
    public function getWhere($key, $value)
    {
        if ($select = $this->table($this->table)->where($key, $value)->limit(1)->select('*')) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }

    /**
     * Get a single value in a row
     * 
     * Return a single vlue as a string.
     *
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Values to limit the query to.
     * @param string $record Specific single value to limit the query to.
     * 
     * @return array
     */
    public function getValueWhere($record, $key, $value)
    {
        if ($select = $this->table($this->table)->where($key, $value)->limit(1)->select($record)) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }

    /**
     * Update a record
     *
     * @param mixed $key Database key / column name to limit the query to.
     * @param mixed $data Update data.
     * 
     * @return mixed         
     */
    public function updateBy($key, $data)
    {
        if ($update = $this->table($this->table)->where($key)->update($data)) {
            if ($update['status'] == 'success' && $update['affected_rows'] > 0) return empty($update['response']) ? true : $update['response'];
            else return false;
        }
    }

    /**
     * Delete a record
     *
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Value/s to delete.
     * 
     * @return mixed
     */
    public function deleteWhere($key, $value)
    {
        if ($delete = $this->table($this->table)->where($key, $value)->delete()) {
            if ($delete['status'] == 'success' && $delete['affected_rows'] > 0) return true;
            else return false;
        }
    }

    /**
     * Cout all records in a table
     *
     * @return mixed
     */
    public function countAll()
    {   
        if ($select = $this->table($this->table)->count()) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }

    /**
     * Count specific records in a table
     *
     * @param string $key Database key / column name to limit the query to.
     * @param mixed $value Values to limit the query to.
     * 
     * @return mixed
     */
    public function countWhere($key, $value)
    {   
        if ($select = $this->table($this->table)->where($key, $value)->count()) {
            if ($select['status'] == 'success') return empty($select['response']) ? false : $select['response'];
            else return false;
        }
    }
}