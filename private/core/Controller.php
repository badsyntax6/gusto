<?php

/**
 * Controller Core Class
 *
 * The controller class is the main controller of the application system.
 * All controller classes will be extensions of this class.
 */
class Controller
{
    /**
     * Controller construct
     * 
     * Controller classes are extended from this class so everytime you 
     * load a controller class this construct will be called.
     * 
     * @return void
     * 
     * @see Core Classes /root/private/core/
     */
    public function __construct()
    {   
        // Does the route require login and is the user logged in?
        if (Auth::requiresLogin() && !Auth::isLogged()) {
            Load::route('/login');
        }   

        // Does the user have sufficient privileges?
        if (Auth::group() < Auth::accessLevel()) {
            Load::route('/status/401');
        }

        // Is the website in maintenance mode?
        if ($this->isMaintenanceMode()) {
            exit(Output::html('information/maintenance', ['message' => Language::get('maintenance/maintenance_mode')]));
        }
    }

    /**
     * Check if maintenance mode is on
     * 
     * Chech for maintenance mode by getting the setting from the settings table. If maintenance mode
     * is on, normal users will be routed to the maintenance view. Administrators will still be able
     * to view all pages.
     * 
     * @return true|false
     */
    public function isMaintenanceMode()
    {   
        $maintenance = Load::model('settings')->getSetting('maintenance_mode');
        if ($maintenance) {
            if (isset(App::$urlArray[0]) && App::$urlArray[0] == 'login' || isset(App::$urlArray[0]) && App::$urlArray[0] == 'logout') return false;
            if (Auth::group() < 4 || is_null(Auth::group())) return true;
        }
        return false;
    }
}
