<?php

/**
 * Sessions Core Class
 *
 * The session class logs user data based on the session user id and 
 * handles session related data.
 */ 
class Session
{
    /**
     * Create a session.
     * 
     * @param string $session_name 
     * @param mixed $session_value 
     */
    public static function create($session_name, $session_value, $override = false)
    {
        if (!isset($_SESSION[$session_name]) || $override) {
            $_SESSION[$session_name] = $session_value;
        }
    }

    public static function get($session_name)
    {
        $session = isset($_SESSION[$session_name]) ? $_SESSION[$session_name] : false;
        return $session;
    }

    public static function delete($session_name)
    {
        unset($_SESSION[$session_name]);
        session_destroy();
    }

    public static function append($session_name, $data)
    {
        if (isset($_SESSION[$session_name])) {
            if (is_array($data)) {
                $_SESSION[$session_name] += $data;
            } else {
                $_SESSION[$session_name] .= $data;
            }
        } else {
            $_SESSION[$session_name] = $data;
        }
    }

    /**
     * Create a cookie.
     * 
     * @param string $name
     * @param mixed $value
     * @param int $expire 
     */
    public static function createCookie($name, $value, $expire)
    {
        setcookie($name, $value, $expire, '/');
    }

    public static function getCookie($cookie_name)
    {
        $cookie = isset($_COOKIE[$cookie_name]) ? $_COOKIE[$cookie_name] : false;
        return $cookie;
    }

    public static function deleteCookie($cookie_name)
    {
        unset($_COOKIE[$cookie_name]);
    }
}