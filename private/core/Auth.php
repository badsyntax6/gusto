<?php

/**
 * Authentication Class
 * 
 * Various methods to check user authentication and privilege. Authentication and
 * routing seem to be tightly coupled so this class extends the Router Class.
 * Perhaps this could change in the future idk.
 * 
 * @see Router Core Class - /root/private/core/Router.php
 */
class Auth extends Router
{
    /**
     * Logged in users data
     * @property object
     */
    public static $user = null;

    /**
     * Check if a user is logged in
     * 
     * @return true|false
     */
    public static function isLogged()
    {
        if (isset($_SESSION['id'])) return true;
        else return false;
    }

    /**
     * Check if the route requires a login
     *
     * @return true|false
     */
    public static function requiresLogin()
    {
        if (parent::$route_level > 0) return true;
        else return false;
    }

    /**
     * Get the route/access level
     *
     * @return int
     */
    public static function accessLevel()
    {
        return parent::$route_level;
    }

    /**
     * Get the logged users data
     * 
     * Get user data from the database if a user is logged in.
     * 
     * @example, Auth::user()->firstname
     * @example, shorter way Auth::firstname()
     * 
     * @see /root/private/models/UserModel.php
     * 
     * @return object
     */
    public static function user()
    {
        if (!self::isLogged()) return;
        if (is_null(self::$user)) self::$user = (object)Load::model('user')->getLoggedUser(Session::get('id'));

        if (isset(self::$user)) return self::$user;
        else return false;
    }

    /**
     * Get the logged users id
     *
     * @example, Auth::id()
     * @return string
     */
    public static function id()
    {
        if (self::user() && isset(self::user()->user_id)) return self::user()->user_id;        
    }

    /**
     * Get the logged users key
     *
     * @example, Auth::key()
     * @return string - complex randomized string unique to each user
     */
    public static function key()
    {
        if (self::user() && self::user()->key) return self::user()->key;
    }

    /**
     * Get the logged users group
     *
     * @example, Auth::group()
     * @return string - The users group or access level. Unregistered users are 1 admins are 4.
     */
    public static function group()
    {
        if (self::user() && isset(self::user()->group)) return self::user()->group;
    }

    /**
     * Get the logged users firstname
     *
     * @example, Auth::firstname()
     * @return string
     */
    public static function firstname()
    {
        if (self::user() && isset(self::user()->firstname)) return self::user()->firstname;
    }

    /**
     * Get the logged users lastname
     *
     * @example, Auth::lastname()
     * @return string
     */
    public static function lastname()
    {
        if (self::user() && isset(self::user()->lastname)) return self::user()->lastname;
    }

    /**
     * Get the logged users full name
     *
     * @example, Auth::fullname()
     * @return string
     */
    public static function fullname()
    {
        if (self::user() && isset(self::user()->firstname) && isset(self::user()->lastname)) return self::user()->firstname . ' ' . self::user()->lastname;
    }

    /**
     * Get the logged users username
     *
     * @example, Auth::username()
     * @return string
     */
    public static function username()
    {
        if (self::user() && isset(self::user()->username)) return self::user()->username;
    }

    /**
     * Get the logged users email address
     *
     * @example, Auth::email()
     * @return string
     */
    public static function email()
    {
        if (self::user() && isset(self::user()->email)) return self::user()->email;
    }
}