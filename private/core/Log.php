<?php

/**
 * Log Class
 * 
 * Event and error messages will be formatted and inserted into log files and/or the datbase.
 */
class Log 
{
    /**
     * Log events
     * 
     * Events are user generated. When a user signs in or out or changes a record ect. 
     * A user made message will be logged. Event messages are stored in the events.log
     * file and in the database log table.
     *
     * @param string $message - Event message.
     * 
     * @return void
     * 
     * @see root/private/logs/events.log
     */
    public static function event($message)
    {
        $date = date('Y-m-d h:i:s');
        error_log($date . ' ' . strip_tags($message) . "\n", 3, PRIVATE_DIR . '/logs/events.log');
        Database::instance()->mysqli->query('INSERT INTO `log` (`time`, `event`) VALUES ("' . $date . '", "' . $message . '")');
    }

    /**
     * Log php errors
     * 
     * When PHP generates an error, catch it and log it in the server.log file.
     *
     * @param string $message - PHP error message string.
     * 
     * @return void
     * 
     * @see root/private/logs/server.log
     */
    public static function server($message)
    {
        error_log(date('Y-m-d h:i:s') . ' Error: ' . strip_tags($message), 3, PRIVATE_DIR . "/logs/server.log");
    }

    /**
     * Log db errors
     * 
     * When mysql generates and error log it in the db.log file.
     *
     * @param string $message - DB query error.
     * 
     * @return void
     * 
     * @see root/private/logs/db.log
     */
    public static function db($message)
    {
        $date = date('Y-m-d h:i:s');
        error_log($date . ' ' . strip_tags($message) . "\n", 3, PRIVATE_DIR . "/logs/db.log");
        Database::instance()->mysqli->query('INSERT INTO `errors` (`time`, `event`) VALUES ("' . $date . '", "' . $message . '")');
    }
}
