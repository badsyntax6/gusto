<?php

/**
 * Core App
 * 
 * @see root/public/index.php
 */
class App
{
    /**
     * Store the single instance of the application.
     * @property object
     */
    private static $instance;

    /**
     * The current url as a string.
     * @property string
     */
    public static $url;

    /**
     * The current url as an array.
     * @property array
     */
    public static $urlArray;

    /**
     * Application Construct
     * 
     * Start the application, include necessary files and instantiate necessary componenent classes.
     * Start the session and handle basic functionality.
     * 
     * @return void
     */
    public function __construct()
    {
        if (!isset($_SESSION)) { session_start(); }
        
        require_once PRIVATE_DIR . '/config/db.php';
        require_once PRIVATE_DIR . '/config/app.php'; 
        require_once PRIVATE_DIR . '/helpers/common.php';
        require_once PRIVATE_DIR . '/helpers/dates.php';

        self::$url = isset($_GET['url']) ? $_GET['url'] : '';
        self::$urlArray = isset($_GET['url']) ? explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL)) : [];

        Router::sort(require_once PRIVATE_DIR . '/routes.php');
    }

    /**
     * Get a template file
     * 
     * @param string $template Sub dir and filename.
     * 
     * @return string The text in the file.
     * 
     * @example, App::getTemplate('email/activate')
     */
    public static function getTemplate($template)
    {
        $file = PRIVATE_DIR . '/storage/templates/' . $template . '.txt';
        ob_start();
        require $file;
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    /**
     * Create a static instance of this class.
     * 
     * @return self
     */
    public static function init() 
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}