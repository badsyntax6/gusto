<?php

/**
 * Get an array of routes
 * 
 * Create and return an array of routes for the core router to process.
 * 
 * @example, Levels
 * 0 = Un-registered / locked
 * 1 = Registered but not yet activated
 * 2 = Registered and activated
 * 3 = Moderator
 * 4 = Admin
 *
 * @see root/private/core/Router.php
 */
$routes = [
    // SIGNUP
    ["route" => "signup",                           "controller" => "SignupController",           "action" => "index",                  "level" => 0],
    ["route" => "signup/validate",                  "controller" => "SignupController",           "action" => "validate",               "level" => 0],
    ["route" => "signup/validate-live",             "controller" => "SignupController",           "action" => "validateLive",           "level" => 0],
    // LOGIN
    ["route" => "login",                            "controller" => "LoginController",            "action" => "index",                  "level" => 0],
    ["route" => "login/validate",                   "controller" => "LoginController",            "action" => "validate",               "level" => 0],
    ["route" => "login/validate-live",              "controller" => "LoginController",            "action" => "validateLive",           "level" => 0],
    // LOGOUT
    ["route" => "logout",                           "controller" => "LogoutController",           "action" => "index",                  "level" => 0],
    ["route" => "logout/destroySession",            "controller" => "LogoutController",           "action" => "destroySession",         "level" => 0],
    ["route" => "logout/inactive",                  "controller" => "LogoutController",           "action" => "inactive",               "level" => 0],
    ["route" => "logout/confirm",                   "controller" => "LogoutController",           "action" => "confirm",                "level" => 0],
    // HOME/DASHBOARD
    ["route" => "/",                                "controller" => "DashboardController",        "action" => "index",                  "level" => 2],
    ["route" => "dashboard",                        "controller" => "DashboardController",        "action" => "index",                  "level" => 2],
    ["route" => "dashboard/clear-log",              "controller" => "DashboardController",        "action" => "clearLog",               "level" => 4],
    ["route" => "dashboard/clear-errors",           "controller" => "DashboardController",        "action" => "clearErrors",            "level" => 4],
    ["route" => "header/get-notifications",         "controller" => "HeaderController",           "action" => "getNotifications",       "level" => 2],
    // SEARCH
    ["route" => "search",                           "controller" => "SearchController",           "action" => "index",                  "level" => 2],
    // HTTP STATUS
    ["route" => "status/{\w+}",                     "controller" => "StatusController",           "action" => "index",                  "level" => 0],
    // ACCOUNT
    ["route" => "account",                          "controller" => "AccountController",          "action" => "index",                  "level" => 2],
    ["route" => "account/getAccountJson",           "controller" => "AccountController",          "action" => "getAccountJson",         "level" => 2],
    ["route" => "account/uploadAvatar",             "controller" => "AccountController",          "action" => "uploadAvatar",           "level" => 2],
    ["route" => "account/validate",                 "controller" => "AccountController",          "action" => "validate",               "level" => 0],
    ["route" => "account/activate",                 "controller" => "AccountController",          "action" => "activate",               "level" => 0],
    ["route" => "account/send",                     "controller" => "AccountController",          "action" => "send",                   "level" => 0],
    ["route" => "account/forgot",                   "controller" => "AccountController",          "action" => "forgot",                 "level" => 0],
    ["route" => "account/send-reset-request",       "controller" => "AccountController",          "action" => "sendResetRequest",       "level" => 0],
    ["route" => "account/reset/{\w+}",              "controller" => "AccountController",          "action" => "reset",                  "level" => 0],
    ["route" => "account/save-new-password",        "controller" => "AccountController",          "action" => "saveNewPassword",        "level" => 0],
    ["route" => "account/validate-live",            "controller" => "AccountController",          "action" => "validateLive",           "level" => 0],
    ["route" => "account/isEmailTaken",             "controller" => "AccountController",          "action" => "isEmailTaken",           "level" => 0],
    ["route" => "account/changeThemeSetting",       "controller" => "AccountController",          "action" => "changeThemeSetting",     "level" => 2],
    // SETTINGS
    ["route" => "settings",                         "controller" => "SettingsController",         "action" => "index",                  "level" => 4],
    ["route" => "settings/update/{\w+}",            "controller" => "SettingsController",         "action" => "update",                 "level" => 4],
    ["route" => "settings/general",                 "controller" => "SettingsController",         "action" => "general",                "level" => 4],
    ["route" => "settings/mail",                    "controller" => "SettingsController",         "action" => "mail",                   "level" => 4],
    ["route" => "settings/server",                  "controller" => "SettingsController",         "action" => "server",                 "level" => 4],
    ["route" => "settings/getSettingsJson",         "controller" => "SettingsController",         "action" => "getSettingsJson",        "level" => 4],
    ["route" => "settings/activity-check",          "controller" => "SettingsController",         "action" => "getInactivityLimit",     "level" => 2],
    ["route" => "settings/updateLastActive",        "controller" => "SettingsController",         "action" => "updateLastActive",       "level" => 2],
    ["route" => "settings/getMenuSetting",          "controller" => "SettingsController",         "action" => "getMenuSetting",         "level" => 2],
    ["route" => "settings/changeMenuSetting",       "controller" => "SettingsController",         "action" => "changeMenuSetting",      "level" => 2],
    ["route" => "settings/updateMailSettings",      "controller" => "SettingsController",         "action" => "updateMailSettings",     "level" => 4],
    ["route" => "settings/test-email",              "controller" => "SettingsController",         "action" => "testEmail",              "level" => 4],
    // USERS
    ["route" => "users",                            "controller" => "UsersController",            "action" => "index",                  "level" => 2],
    ["route" => "users/view",                       "controller" => "UsersController",            "action" => "index",                  "level" => 2],
    ["route" => "users/user",                       "controller" => "UsersController",            "action" => "user",                   "level" => 2],
    ["route" => "users/user/{[a-z\-]+}",            "controller" => "UsersController",            "action" => "user",                   "level" => 2],
    ["route" => "users/getTable",                   "controller" => "UsersController",            "action" => "getTable",               "level" => 2],
    ["route" => "users/edit",                       "controller" => "UsersController",            "action" => "edit",                   "level" => 4],
    ["route" => "users/delete",                     "controller" => "UsersController",            "action" => "delete",                 "level" => 4],
    ["route" => "users/get-user-numbers",           "controller" => "UsersController",            "action" => "getUserNumbers",         "level" => 4],
    // CALENDAR
    ["route" => "calendar",                         "controller" => "CalendarController",         "action" => "index",                  "level" => 2],
    ["route" => "calendar/new",                     "controller" => "CalendarController",         "action" => "add",                    "level" => 2],
    ["route" => "calendar/get-events",              "controller" => "CalendarController",         "action" => "getEvents",              "level" => 2],
    ["route" => "calendar/getEvent",                "controller" => "CalendarController",         "action" => "getEvent",               "level" => 2],
    ["route" => "calendar/getEventJson/{\d+}",      "controller" => "CalendarController",         "action" => "getEventJson",           "level" => 2],
    ["route" => "calendar/getCalendar",             "controller" => "CalendarController",         "action" => "getCalendar",            "level" => 2],
    ["route" => "calendar/edit/{\d+}",              "controller" => "CalendarController",         "action" => "edit",                   "level" => 2],
    ["route" => "calendar/update",                  "controller" => "CalendarController",         "action" => "update",                 "level" => 2],
    ["route" => "calendar/save",                    "controller" => "CalendarController",         "action" => "save",                   "level" => 2],
    ["route" => "calendar/delete",                  "controller" => "CalendarController",         "action" => "deleteEvent",            "level" => 2],
    // FILEBROWSER
    ["route" => "file-browser",                     "controller" => "FilebrowserController",      "action" => "index",                  "level" => 0],
    ["route" => "filebrowser/browse",               "controller" => "FilebrowserController",      "action" => "browse",                 "level" => 0],
    ["route" => "filebrowser/makeFolder",           "controller" => "FilebrowserController",      "action" => "makeFolder",             "level" => 0],
    ["route" => "filebrowser/delete",               "controller" => "FilebrowserController",      "action" => "delete",                 "level" => 0],
    ["route" => "filebrowser/upload",               "controller" => "FilebrowserController",      "action" => "upload",                 "level" => 0],
    // PAGINATION
    ["route" => "pagination",                       "controller" => "PaginationController",       "action" => "index",                  "level" => 2],
    ["route" => "pagination/getTable",              "controller" => "PaginationController",       "action" => "getTable",               "level" => 2],
    // TEST
    ["route" => "test",                             "controller" => "TestController",             "action" => "index",                  "level" => 0],
    ["route" => "test/test",                        "controller" => "TestController",             "action" => "testInput",              "level" => 0],
    ["route" => "test/validate",                    "controller" => "TestController",             "action" => "validate",               "level" => 0],
    ["route" => "test/countdown",                   "controller" => "TestController",             "action" => "countdown",              "level" => 0],
    ["route" => "test/checkDay",                    "controller" => "TestController",             "action" => "checkDay",               "level" => 0],
    ["route" => "test/sql",                         "controller" => "TestController",             "action" => "sql",                    "level" => 0],
    ["route" => "test/{\w+}/{\w+}/{\w+}",           "controller" => "TestController",             "action" => "index",                  "level" => 0],
    ["route" => "test/get-users",                   "controller" => "TestController",             "action" => "getUserJson",            "level" => 0]
];

return $routes;