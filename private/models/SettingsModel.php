<?php 

/**
 * Settings Model Class
 *
 * Interact with the database to process data related to the settings.
 */
class SettingsModel extends Model
{
    /**
     * Get all settings data
     *
     * Get all settingss data and return their data in arrays.
     * @return array
     */
    public function getSettings()
    {
        $select = $this->table('settings')->select('*');
        return $this->selectResponse($select);
    }

    public function getSetting($data)
    {
        $select = $this->table('settings')->where('name', $data)->limit(1)->select('value');
        return $this->selectResponse($select);
    }

    public function updateSetting($data)
    {
        $update = $this->table('settings')->where('setting_id')->update($data);
        return $this->updateResponse($update, false);
    }

    public function getMailSettings()
    {
        $select = $this->table('mail')->limit(1)->select('*');
        return $this->selectResponse($select);
    }

    public function insertMailSettings($data)
    {
        $insert = $this->table('mail')->insert($data);
        return $this->insertResponse($insert);
    }

    public function updateMailSettings($data)
    {
        $update = $this->table('mail')->where('mail_id')->update($data);
        return $this->updateResponse($update, false);
    }

    public function getLanguages()
    {
        $select = $this->table('language')->select('*');
        return $this->selectResponse($select);
    }
}