<?php 

/**
 * Search Model Class
 *
 * This class interact with the database and tries to find data that matches 
 * the users search string.
 */
class SearchModel extends Model
{
    public function searchUsers($data)
    {
        $select = $this->table('user')->whereLike('firstname', $data)->orWhereLike('lastname', $data)->orWhereLike('email', $data)->select('firstname, lastname, email, user_id');
        return $this->selectResponse($select);
    }
}