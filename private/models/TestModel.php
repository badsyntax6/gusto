<?php 

/**
 * Test Model
 *
 * 
 */
class TestModel extends Model
{
    public function getRecord()
    {
        if ($select = $this->table('user')->where('user_id', 1)->select('firstname, lastname')) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getRecords()
    {
        $select = $this->table('user')->select('firstname');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getRecordsNum()
    {
        $select = $this->table('user')->where('firstname', 'Chase')->count();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
                
            } else {
                return false;
            }
        }
    }

    public function insertRecord($post)
    {
        $insert = $this->table('logins')->insert($post);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    public function updateRecord($post)
    {   
        $update = $this->table('user')->where('email')->update($post);
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function massUpdate($post)
    {   
        $update = $this->table('user')->where('user_id')->andWhere('user_id')->andWhere('user_id')->update($post);
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function deleteRecord() {
        if ($delete = $this->table('user')->where('user_id', 1)->delete()) {
            if ($delete['status'] == 'success') {
                return empty($delete[1]) ? true : $delete[1];
            } else {
                return false;
            }
        }
    }
}
