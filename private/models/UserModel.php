<?php 

/**
 * User Model Class
 *
 * Interact with the database to process data related to the users.
 */
class UserModel extends Model
{
    public $search_string = '';
    /**
     * Get a single users data
     *
     * Get a users data and return their data in an array.
     * @return array
     */
    public function getLoggedUser($key)
    {
        $select = parent::leftJoin('menus', 'user_id', 'menu_anchor')->where('key', $key)->limit(1)->select('*');
        return $this->selectResponse($select);
    }

    public function getUserProfile($firstname, $lastname)
    {
        $select = parent::where('firstname', $firstname)->andWhere('lastname', $lastname)->limit(1)->select('*');
        return $this->selectResponse($select);
    }

    public function getLoginAttempts($is, $email)
    {
        $select = $this->table('logins')->where('ip', $is)->andWhere('email', $email)->limit(1)->select('*');
        return $this->selectResponse($select);
    }

    public function updateLoginAttempts($data)
    {
        $update = $this->table('logins')->where('email')->andWhere('ip')->update($data);
        return $this->updateResponse($update);
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param array $post      
     * @return bool   
     */
    public function createUserMenuSetting($user_id)
    {
        $data['menu_anchor'] = $user_id;
        $data['main_menu'] = 0;

        $insert = $this->table('menus')->insert($data);
        return $this->insertResponse($insert);
    }

    public function getUsersForPagination($settings)
    {   
        $start = ($settings['page']-1) * $settings['limit'];

        if (isset($settings['search_string']) && !empty($settings['search_string'])) {
            $select = $this->table($settings['table'])
            ->whereLikeArray($settings['search_keys'], $settings['search_string'])
            ->orderBy($settings['orderby'], $settings['direction'])
            ->limitBetween($start, $settings['limit'])
            ->selectDistinct('*');
            $count = $this->table($settings['table'])
            ->whereLikeArray($settings['search_keys'], $settings['search_string'])
            ->limitBetween(0, $settings['limit'])
            ->countDistinct();
        } else {
            $select = $this->table($settings['table'])->orderBy($settings['orderby'], $settings['direction'])->limitBetween($start, $settings['limit'])->select('*');
            $count = $this->table($settings['table'])->count();
        }

        $total = $count['response'];

        if ($select && $select['status'] == 'success') {
            return empty($select['response']) ? [] : ['settings' => $settings, 'records' => $select['response'], 'total' => $total, 'start' => $start, 'query' => $select['query']];
        } else {
            return false;
        }
    }
}