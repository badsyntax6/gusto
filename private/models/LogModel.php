<?php 

/**
 * Log Model Class
 *
 * Interact with the database to process data related to the log.
 */
class LogModel extends Model
{
    /**
     * Insert a new log record into the database.
     *
     * @param array $post      
     * @return bool   
     */
    public function insertLog($string)
    {
        $data['time'] = date('c');
        $data['event'] = $string;
        $insert = parent::insert($data);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Get all log records.
     *
     * @param string $column
     * @param mixed $is
     * @return mixed
     */
    public function getLogs()
    {
        $select = parent::orderby('log_id', 'desc')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? [] : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Delete all log records.
     *
     * @return bool
     */
    public function clearLog()
    {
        if (parent::truncate()) return true;
        else return false;
    }

    /**
     * Insert a new log record into the database.
     *
     * @param array $post      
     * @return bool   
     */
    public function insertError($string)
    {
        $data['time'] = date('c');
        $data['event'] = $string;
        $insert = $this->table('error')->insert($data);
        return $this->insertResponse($insert);
    }

    /**
     * Get all error records.
     *
     * @return mixed
     */
    public function getErrors()
    {
        $select = $this->table('errors')->orderby('error_id', 'desc')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? [] : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Delete all error records.
     *
     * @return void
     */
    public function clearErrors()
    {
        if ($this->table('errors')->truncate()) return true;
        else return false;
    }
}