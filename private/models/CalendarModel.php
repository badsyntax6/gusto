<?php

class CalendarModel extends Model
{
    /**
     * Get all calendar events
     *
     * @param string $month - Should be a month and year. @example, 2019-09
     * @return mixed If there is data return it, if not return false
     */
    public function getEvents($date)
    {
        $select = $this->table('calendar')->whereMonth('start_time', $date)->select('*');
        return $this->selectResponse($select);
    }
}
