<?php


/**
 * Explode a url
 * 
 * Gets the url from the site and explodes it into pieces. Good for when you want to use 
 * the url like an array. website/page/param becomes array('website', 'page', 'param').
 *
 * @param string $url Expects the current URL.
 * 
 * @return array The URL as an array.
 */
function explodeUrl($url)
{
    $url = explode('/', filter_var(trim($url, '/'), FILTER_SANITIZE_URL));
    return $url;
}

/**
 * Get a list of files and folders
 * 
 * @param string $dir Directory to scan.
 * 
 * @return array Files and folders found in dir.
 */
function getFileList($dir)
{
    $dir .= '/';
    $found = array_diff(scandir($dir), ['.', '..', 'index.htm']);

    foreach ($found as $f) {
        if (is_file($dir . $f)) $files[] = $f;
    }

    natcasesort($files);

    return $files;
}

/**
 * Bot test
 * 
 * Check the "red_herring" input to see if it is filled. If the "red_herring" input 
 * is not empty then exit immediately. The assumption being that only bots would fill 
 * out this field.
 *
 * @param mixed - $input - The red herring post input.
 * 
 * @return void
 */
function botTest($input) 
{
    if (!empty($input)) exit();
}

/**
 * Clean an array
 * 
 * Cleans up sloppy arrays by performing a few actions on them. Such as removing whitespaces from
 * the begginings and ends of values, removing duplicate values, and sorting the array.
 *
 * @param array $array
 * 
 * @return void
 */
function cleanArray($array)
{
    $a = array_unique(array_filter(array_map('trim', $array)));
    sort($a);
    return $a;
}

/**
 * Overwrite array
 * 
 * Compare an array against another and if matching keys are found, overwrite the 
 * data in the first array with the data in the second array.
 *
 * @param array $ar1 The array that will be overwritten and returned.
 * @param array $ar2 An array with values to overwrite the first array.
 * 
 * @return array
 */
function rewriteArray($ar1 = [], $ar2 = [])
{
    $keys = array_intersect_key($ar1, $ar2);
    foreach ($keys as $key => $value) {
        $ar1[$key] = $ar2[$key];
    }

    return $ar1;
}

/**
 * Get file size
 * 
 * Return a readable file size by doing math on bytes. Note that because the 
 * bytes are divided by 1000, the sizes are KB, MB, GB. If the division was
 * done by 1024 the sizes would be KiB, MiB, GiB.
 *
 * @param int $bytes Number of bytes to calculate.
 * @param integer $precision IDK
 * 
 * @return void
 * 
 * @see https://stackoverflow.com/questions/2510434/format-bytes-to-kilobytes-megabytes-gigabytes
 */
function getFileSize($bytes, $precision = 2) 
{ 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1000)); 
    $pow = min($pow, count($units) - 1); 
    $bytes /= pow(1000, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 

/**
 * Get directory size
 * 
 * Return the size of a directory. 
 *
 * @param string $directory Name of directory.
 * 
 * @return string
 * 
 * @see https://stackoverflow.com/questions/478121/how-to-get-directory-size-in-php
 */
function getDirSize($directory) 
{
    $size = 0;
    $files = glob($directory . '/*');
    foreach ($files as $path) {
        is_file($path) && $size += filesize($path);
        is_dir($path) && getDirSize($path);
    }
    return $size;
} 

/**
 * Paginate
 *
 * @param array $data Multi array of data to be paginated
 * 
 * @return array
 */
function paginate($data) 
{
    if (!isset($data)) return [];

    $records = $data['records'];
    $settings = $data['settings'];
    $orderby = $settings['orderby'];

    if ($settings['direction'] == 'asc') {
        usort($records, function($a, $b) use ($orderby) {
            return $a[$orderby] <=> $b[$orderby];
        });
    }

    if ($settings['direction'] == 'desc') {
        usort($records, function($a, $b) use ($orderby) {
            return $b[$orderby] <=> $a[$orderby];
        });
    }

    $pages = ceil($data['total'] / $settings['limit']);

    return ['pages' => (int) $pages, 'records' => $records, 'total' => (int) $data['total'], 'start' => (int) $data['start'], 'limit' => $settings['limit'], 'query' => $data['query']];  
}