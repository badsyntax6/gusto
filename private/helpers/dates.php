<?php

/**
 * Get the ammount of time that has passed
 * 
 * Get the ammount of time that has passed in full down to the second or get just a
 * single maximum interval of time passed.
 *
 * @param string|int $date Date to compare to, can be date string or timestamp.
 * @param boolean $full Set to full for the full time down to the second.
 * 
 * @return string
 */
function getTimeAgo($date = '2019-04-3 10:13:09.000000', $full = true) 
{
    if (is_numeric($date)) $date = date('c', (int)$date);
    
    $time = new DateTime($date);
    $today = new DateTime('now');

    if ($today->getTimestamp() < $time->getTimestamp()) return 'The date (' . $date . ') is not valid. It is not a past date or not properly formatted';

    $time = $time->diff($today);
    $parts = ['y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second'];

    foreach ($parts as $key => &$value) {
        if ($time->$key !== 0) {
            $value = $time->$key . ' ' . ucfirst($value) . ($time->$key > 1 ? 's' : '');
        } else {
            unset($parts[$key]);
        }
    }

    if (!$full) $parts = array_slice($parts, 0, 1);

    return $parts ? implode(', ', $parts) . ' ago' : 'just now';
}

/**
 * Turn a date into an array
 *
 * @param string $date
 * @return array
 */
function dateToArray($date)
{
    $date = date('D-l-d-m-M-F-m-y-Y-g:ia-G:ia', strtotime($date));
    $date = explode('-', $date);

    $array['day'] = $date[0];
    $array['day_short'] = $date[1];
    $array['day_num'] = $date[2];
    $array['month'] = $date[3];
    $array['month_short'] = $date[4];
    $array['month_num'] = $date[5];
    $array['year'] = $date[7];
    $array['year_short'] = $date[8];
    $array['time'] = $date[9];
    $array['time_24'] = $date[10];

    return $array;
}

/**
 * Countdown to a specific time
 *
 * @param mixed $date Date to countdown to. Can be unix timestmp or date string.
 * @return array|false
 */
function countdown($date = 'December 25, 2020 12:00 am', $full = true)
{
    if (is_numeric($date)) $date = date('c', (int)$date);
    
    $time = new DateTime($date);
    $today = new DateTime('now');

    if ($today->getTimestamp() > $time->getTimestamp()) return 'The date (' . $date . ') is not valid. It is not a future date or not properly formatted';

    $time = $time->diff($today);
    $parts = ['y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second'];

    foreach ($parts as $key => &$value) {
        if ($time->$key !== 0) {
            $value = $time->$key . ' ' . ucfirst($value) . ($time->$key > 1 ? 's' : '');
        } else {
            unset($parts[$key]);
        }
    }

    if (!$full) $parts = array_slice($parts, 0, 1);

    return $parts ? implode(', ', $parts) . ' left' : 'Now';
}