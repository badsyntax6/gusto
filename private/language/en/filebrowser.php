<?php 

/**
 * Alerts
 */
$_['folder_created'] = 'Your folder has been created.';
$_['dir_exists'] = 'A folder with that name already exists.';
$_['file_deleted'] = 'The selected files have been deleted.';
$_['system_file'] = ' This file cannot be deleted.';
$_['file_exists'] = ' A file with the name {{name}} alraedy exists.';
// File Uploads
$_['name_invalid'] = 'Name invalid. Allowed characters 20 or less and [A-Z, 0-9, _-]';
$_['extension_invalid'] = 'File denied. Excepted files types are: {{types}}.';
$_['too_big'] = 'File is too big {{size}}. Max filesize: {{maxsize}}.';
$_['failure'] = 'Your file could not be uploaded for unknown reasons.';
$_['file_uploaded'] = 'Your file has been uploaded.';