<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

/**
 * Mail Core Class
 *
 * Mail library requires the phpmailer library and is used to send emails
 * from the site.
 * 
 * @example,
 * //Server settings
 * $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
 * $mail->isSMTP();                                            // Send using SMTP
 * $mail->Host       = 'smtp1.example.com';                    // Set the SMTP server to send through
 * $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
 * $mail->Username   = 'user@email.com';                       // SMTP username
 * $mail->Password   = 'secret';                               // SMTP password
 * $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
 * $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
 *
 * //Recipients
 * $mail->setFrom('from@email.com', 'Mailer');
 * $mail->addAddress('joe@email.net', 'Joe User');     // Add a recipient
 * $mail->addAddress('ellen@email.com');               // Name is optional
 * $mail->addReplyTo('info@email.com', 'Information');
 * $mail->addCC('cc@email.com');
 * $mail->addBCC('bcc@email.com');
 *
 * // Attachments
 * $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
 * $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
 *
 * // Content
 * $mail->isHTML(true);                                  // Set email format to HTML
 * $mail->Subject = 'Here is the subject';
 * $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
 * $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
 */
class Mail
{
    public $mailer;

    public function __construct()
    {
        require_once VENDORS_DIR . '/phpmailer/src/PHPMailer.php';
        require_once VENDORS_DIR . '/phpmailer/src/SMTP.php';
        require_once VENDORS_DIR . '/phpmailer/src/Exception.php';

        $this->mailer = new PHPMailer(true);
        $this->mailer->isSMTP();                                // Send using SMTP
        $this->mailer->SMTPAuth = true;                         // Enable SMTP authentication
        $this->mailer->SMTPSecure = 'tls';                      // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $this->mailer->SMTPAutoTLS = false;
        $this->mailer->isHTML(true);  
    }

    public function setSmtpSettings($mail) 
    {                          
        $this->mailer->Host = $mail['host'];                    // Set the SMTP server to send through
        $this->mailer->Username = $mail['username'];            // SMTP username
        $this->mailer->Password = $mail['password'];            // SMTP password
        $this->mailer->Port = $mail['port'];                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    }

    public function sendMultipleHidden($recipients)
    {
        foreach ($recipients as $name => $email) {
            if (is_string($name)) $this->mailer->addBCC($email, $name);
            else $this->mailer->addBCC($email);
        }
        return $this->send();
    }

    public function sendMultiple($recipients)
    {
        foreach ($recipients as $name => $email) {
            if (is_string($name)) $this->mailer->addAddress($email, $name);
            else $this->mailer->addAddress($email);
        }
        return $this->send();
    }

    /**
     * Send email.
     * 
     * @return bool             
     */
    public function send()
    {
        try {                      
            $this->mailer->send();
            return true;
        } catch (Exception $e) {
            return false;
            // echo $this->mailer->ErrorInfo;
        }
    }
}