class Test {
    constructor(settings) {
        this.settings = extend({
            el: 'body',
            name: 'Chase',
            age: 32,
            married: false
        }, settings);
    }

    sayName() {
        const {name, age, married, el} = this.settings;

        document.querySelector(el).addEventListener('click', () => console.log(this.settings.name))

        document.querySelector(el).addEventListener('click', function() {
            console.log(this.settings.name);
        })

        console.log(`Hello my name is ${name} and I am ${age} years old.`);
    }
}

const test = new Test('Chase', 32);

test.sayName();

