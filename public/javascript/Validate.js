class Validate {

    constructor() {
        // console.log('Validate Started');
    }

    init(settings) {
        this.settings = extend({
            form: 'form',
            url: null,
            btn: '.btn',
            pw_str: true,
            required: true,
            listeners: ['blur']
        }, settings);

        this.listen();
    }

    async get() {
        const {form, url} = this.settings;
        const form_el = document.querySelector(form);
        const input = new FormData(form_el);
        const response = await fetch(url, { method: 'POST', body: input });

        try {
            if (response.ok) return await response.json();
            else return response;
        } catch (error) {
            return {};
        }
    }

    async listen() {
        const inputs = document.querySelectorAll('input, select, textarea');
        inputs.forEach(e => {
            this.settings.listeners.forEach(v => {
                e.addEventListener(v, () => this.adjustElements(e), { capture: true });
            });

            e.addEventListener('mousedown', () => {
                const alert = e.parentElement.querySelector('.alert-attached');
                if (alert) {
                    e.classList.remove('error');
                    alert.remove();
                }
            });

            const password = document.querySelector('input[type="password"]');
            e.addEventListener('keyup', () => this.checkPasswordStrength(password));
        });
    }

    async adjustElements(el) {
        let response = await this.get();
        let alerts = document.querySelectorAll('.alert-attached');
        if (alerts.length > 0) alerts.forEach(e => e.remove());
        el.classList.remove('error');
        if (response.errors) {
            Object.entries(response.errors).forEach(v => {
                let input_name = document.querySelector(`input[name="${v[0]}"]`);
                input_name.classList.add('error');
                input_name.insertAdjacentHTML('afterend', `<div class="alert-attached">${v[1]}</div>`);
            });
        }
        this.checkRequired();
    }

    checkPasswordStrength(password) {
        if (!document.querySelector('.password-strength')) {
            password.insertAdjacentHTML('afterEnd', '<strong class="password-strength"></strong>');
        }

        const pw_str = document.querySelector('.password-strength');
        if (this.settings.pw_str && password.value.length > 0) {
            pw_str.innerText = '';
            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(password.value) == false) {
                pw_str.innerText = 'WEAK';
                pw_str.style.color = '#ff6868';
            }
            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(password.value) == true) {
                pw_str.innerText = 'STRONG';
                pw_str.style.color = '#5aa055';
            }
            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{13,}$/.test(password.value) == true) {
                pw_str.innerText = 'VERY STRONG';
                pw_str.style.color = '#5aa055';
            }
        } else {
            if (pw_str) pw_str.remove();
        }
    }

    checkRequired() {
        if (this.settings.required) {
            setTimeout(() => {
                let complete = true;
                const required = document.querySelectorAll('[required]');
                const button = document.querySelector(this.settings.btn);

                required.forEach(v => {
                    if (v.disabled == false && v.value == '') complete = false;
                });

                const errors = document.querySelectorAll('.error');
                if (errors.length > 0) complete = false;
                if (complete == true) button.disabled = false;
                if (complete == false) button.disabled = true;
            }, 100);
        }
    }
}

const validate = new Validate();