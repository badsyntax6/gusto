/**
 * Filebrowser
 * 
 * This class can browse and navigate file systems on the host machine.
 * 
 * @example 
 * filebrowser.init({
 *     dir: '/uploads/'
 * });
 */
class Filebrowser {
    constructor() {
        // console.log('Filebrowser started');
    }

    /**
     * Initialize Filebrowser
     * 
     * Get the user settings and render the initial view, then start retrieving 
     * the files and directories from the file system. Also listen for events.
     * 
     * @param {object} settings
     * @returns {void}
     */
    async init(settings) {

        this.settings = this.extend({
            container: '#filebrowser',
            dir: null,
            allow_new_dirs: true,
            allow_delete: true,
            allow_upload: true,
            allow_navigation: true
        }, settings);

        this.initial_dir = this.settings.dir;
        this.upload_dir = this.settings.dir;

        this.items = [];
        this.cancel = false;
        
        try {
            this.renderInitialView();
            this.renderDirsAndFiles();
            this.listenForEvents();
            this.upload();
            return this.settings;
        } catch (error) {
            return error;
        }
    }

    /**
     * Render the initial view
     * 
     * Draw the initial view with no files or dirs. Just draw the necessary visual 
     * components like the buttons and title. The container will populate with files
     * and dirs in the renderDirsAndFiles() method.
     * 
     * @returns {void}
     */
    renderInitialView() {
        let filebrowserHTML = `<div class="filebrowser">
                                    <div class="fb-titlebar">`;                          
        if (this.settings.allow_navigation) {
            filebrowserHTML += `<button type="button"class="btn fb-btn-back no-load" title="Back"><i class="fas fa-level-up-alt fa-fw"></i></button>`;
        }                          
        filebrowserHTML +=  `<button type="button" class="btn fb-btn-upload-close no-load" title="Cloase Upload Panel" style="display: none;">
                                <i class="fas fa-chevron-down fa-fw"></i> Close Uploader
                            </button>
                            <span class="fb-title"></span>
                            </div>
                                <div class="fb-content">
                                    <div class="fb-fileview">
                                        <div class="fb-actions">
                                        <button type="button" class="btn btn-default fb-btn-refresh no-load" title="Refresh"><i class="fas fa-sync-alt fa-fw"></i> Refresh</button>`;
                                        
        if (this.settings.allow_upload) {
            filebrowserHTML += '<button type="button" class="btn btn-basic fb-btn-upload-open no-load" title="Upload"><i class="fas fa-upload fa-fw"></i> Upload</button><hr>';
        }
        if (this.settings.allow_new_dirs) {
            filebrowserHTML += `<form id="new-folder-form">
                                    <input type="text" name="foldername" class="fb-new-folder-input" value="New Folder" style="width: 500px;">
                                    <button type="button" class="btn btn-go fb-btn-make-folder btn-attached-left"><i class="fas fa-save fa-fw"></i> Save</button>
                                </form>
                                <hr>`
        }
        if (this.settings.allow_delete) {
            filebrowserHTML += '<button type="button" class="btn btn-stop fb-btn-mass-delete no-load" title="Delete"><i class="fas fa-trash fa-fw"></i> Delete</button>';
        }
        filebrowserHTML += `</div>
                                <table class="fb-table">
                                    <tr>
                                        <th class="check-cell"><input type="checkbox" id="check-all"></th>
                                        <th>Name</th>
                                        <th>Size</th>
                                        <th>Date Modified</th>
                                    </tr>
                                </table>
                            </div>
                            <div class="fb-upload">
                                <form class="fb-upload-form">
                                    <label for="fb-files"><span>Drag files here <br> or click to select</span> <input type="file" name="fb-files" class="fb-files" multiple></label>
                                    <div class="fb-upload-controls">
                                        <button type="button" class="fb-cancel-upload"><i class="fas fa-eraser fa-fw"></i> Clear</button>
                                        <button type="button" class="fb-commit-upload"><i class="fas fa-upload fa-fw"></i> Commit</button>
                                    </div>
                                </form>
                                <ul class="fb-preview"></ul>
                            </div>
                        </div>
                    </div>`;                                
        document.querySelector(this.settings.container).insertAdjacentHTML('afterBegin', filebrowserHTML);
        document.querySelector('.fb-title').innerText = this.settings.dir;

        let container = document.querySelector(this.settings.container);
        let height = container.parentElement.offsetHeight;
        container.style.height = `${height}px`;
    }

    /**
     * Get files and dirs
     * 
     * Using Javascripts fetch api get a json object of files and directories
     * and return it.
     * 
     * @see root/private/controllers/FilebrowserController.php
     * @see root/private/libraries/Filebrowser.php
     * 
     * @returns {object}
     */
    async getFiles() {
        const response = await fetch('/filebrowser/browse', {
            method: 'POST',
            headers: {'Content-type': 'application/x-www-form-urlencoded'},
            body: new URLSearchParams({dir: this.settings.dir})
        });

        if (response.ok) return await response.json();
    }

    /**
     * Render the files and directories
     * 
     * After the initial view is rendered and the file system data is retrieved
     * draw them to the page inside the filebrowser container.
     * 
     * @returns {void}
     */
    renderDirsAndFiles() {
        return new Promise(async (resolve, reject) => {
            const data = await this.getFiles(this.settings.dir);
            if (!data || data.length <= 0) return reject('No files or dirs found');
            const {dirs, files} = data;

            // This method will be called repeatedly so removed the rows so new refreshed rows can be added
            document.querySelectorAll('.fb-row').forEach(v => v.remove());

            // If its not the first time this method is called alerts may need to be cleared
            const alert = document.querySelector('.alert');
            if (alert) alert.remove();

            const table = document.querySelector('.fb-table');

            dirs.forEach(v => {
                const row = table.insertRow(-1);
                const cell1 = row.insertCell(0);
                const cell2 = row.insertCell(1);
                const cell3 = row.insertCell(2);
                const cell4 = row.insertCell(3);
                row.classList.add('fb-row');
                cell1.innerHTML = `<input type="checkbox" name="${v.name}" value="${v.name}" class="checkbox">`;
                cell1.classList.add('text-center', 'check-cell');
                cell2.innerHTML = `<button type="button" class="fb-dir-link"><i class="fas fa-folder fa-fw"></i>${v.name}</button>`;
                cell2.classList.add('fb-item', 'fb-dir');
                cell3.innerHTML = v.size;
                cell4.innerHTML = v.modified;
            });
            files.forEach(v => {
                const row = table.insertRow(-1);
                const cell1 = row.insertCell(0);
                const cell2 = row.insertCell(1);
                const cell3 = row.insertCell(2);
                const cell4 = row.insertCell(3);
                row.classList.add('fb-row');
                cell1.innerHTML = `<input type="checkbox" name="${v.name}" value="${v.name}" class="checkbox">`;
                cell1.classList.add('text-center', 'check-cell');
                cell2.innerHTML = `<td class="fb-item fb-file"><button type="button" class="fb-file-link">${v.name}</button>`;
                cell2.classList.add('fb-item', 'fb-file');
                cell3.innerHTML = v.size;
                cell4.innerHTML = v.modified;
            });

            document.querySelectorAll('.fb-file-link').forEach(v => {
                const filename = v.innerText;
                const extension = filename.substr((filename.lastIndexOf('.') +1));
                const ext_ico = this.getExtensionIcon(extension);
                switch (extension) {
                    case 'jpg':
                    case 'JPG':
                    case 'jpeg':
                    case 'JPEG':
                    case 'png':
                    case 'PNG':
                    case 'gif':
                    case 'GIF':
                        v.insertAdjacentHTML('afterbegin', `<img src="${this.settings.dir}thumb_${filename}" alt="${filename}" class="fb-thumbnail">`);
                    break;
                    default:
                        v.insertAdjacentHTML('afterbegin', ext_ico);
                    break;
                }
            });
            return resolve('Files and dirs rendered');
        });
    }

    /**
     * Get icons for extension types
     *  
     * Return a fontawesome element based on file extension.
     * 
     * @param {string} extension 
     * @return {string}
     */
    getExtensionIcon(extension) {
        switch (extension) {                       
            case 'zip':
            case 'rar': return '<i class="fas fa-file-archive fa-fw"></i>'; break;
            case 'pdf': return '<i class="far fa-file-pdf fa-fw"></i>'; break;
            case 'htm':
            case 'html': return '<i class="fab fa-html5 fa-fw"></i>'; break;
            case 'php': return '<i class="fas fa-code fa-fw"></i>'; break;
            case 'css': return '<i class="fas fa-css3 fa-fw"></i>'; break;
            case 'xlsx': return '<i class="fas fa-file-excel fa-fw"></i>'; break;
            case 'docx': return '<i class="fas fa-file-word fa-fw"></i>'; break;
            case 'txt': return '<i class="fas fa-file-word fa-fw"></i>'; break;
            default: return '<i class="fas fa-question-circle fa-fw"></i></i>';
        }
    }

    /**
     * Listen for user input
     * 
     * All methods that listen for user input are called here.
     * 
     * @returns {void}
     */
    listenForEvents() {
        this.refresh();

        if (this.settings.allow_navigation) {
            this.descend();
            this.ascend();
        }

        if (this.settings.allow_new_dirs) this.makeFolder();
        if (this.settings.allow_delete) this.delete();
        if (this.settings.allow_upload) this.toggleUploadPanel();
    }

    /**
     * Descend into a directory
     * 
     * @returns {void}
     */
    descend() {
        document.addEventListener('click', async (e) => {
            if (e.target.classList.contains('fb-dir-link') && this.settings.allow_navigation) {
                e.target.disabled = true;
                this.settings.dir = this.settings.dir + e.target.innerText.trim() + '/'
                this.renderDirsAndFiles();
                document.querySelector('.fb-title').innerText = this.settings.dir;
                document.querySelector('.fb-btn-back').style.display = 'inline-block';
            }
        });
    }

    /**
     * Ascend out of a directory
     * 
     * @returns {void}
     */
    ascend() {
        document.addEventListener('click', async (e) => {
            if (e.target.parentElement.classList.contains('fb-btn-back') || e.target.classList.contains('fb-btn-back')) {
                if (e.target.parentElement.classList.contains('fb-btn-back')) e.target.parentElement.disabled = true;
                if (e.target.classList.contains('fb-btn-back')) e.target.disabled = true;
                let dir = this.settings.dir.split('/').slice(0, -2).join('/');
                this.settings.dir = dir + '/';
                this.renderDirsAndFiles();
                document.querySelector('.fb-title').innerText = this.settings.dir;
                if (this.initial_dir == this.settings.dir) document.querySelector('.fb-btn-back').style.display = 'none';
                if (e.target.parentElement.classList.contains('fb-btn-back')) e.target.parentElement.disabled = false;
                if (e.target.classList.contains('fb-btn-back')) e.target.disabled = false;
            }
        });
    }

    /**
     * Make a new directory
     * 
     * @returns {void}
     */
    makeFolder() {
        const button = document.querySelector('.fb-btn-make-folder');
        button.addEventListener('click', async () => {
            button.disabled = true;
            document.querySelector('.filebrowser').insertAdjacentHTML('afterBegin', '<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
            const dir = this.settings.dir;
            const foldername = document.querySelector('.fb-new-folder-input').value;
            const form_data = new URLSearchParams({dir:dir,foldername:foldername}).toString();
            const response = await fetch('/filebrowser/makeFolder', {
                method: 'POST',
                headers: {'Content-type': 'application/x-www-form-urlencoded'},
                body: form_data
            });
            if (response.ok) {
                let data = await response.json();
                document.querySelector('.loading').remove();
                await this.renderDirsAndFiles()
                .then(response => {
                    console.log(response);
                    document.querySelector('.fb-actions').insertAdjacentHTML('afterend', `<div class="alert ${data.alert}"><strong>${data.alert}!</strong> ${data.message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>`);
                })
                .catch(error => console.log(error));
            }
            button.disabled = false;
        });       
    }

    /**
     * Delete a file or directory
     * 
     * @returns {void}
     */
    delete() {
        document.querySelector('.fb-btn-mass-delete').addEventListener('click', async () => {
            if (confirm('All selected files will be permanently deleted. Are you sure you want to continue?')) {
                let formdata = new FormData();
                const names = document.querySelectorAll('.checkbox');

                names.forEach(v => {
                    if (v.checked) formdata.append(`filenames[${v.name}]`, v.value);                    
                });
                formdata.append('dir', this.settings.dir);

                let data = new URLSearchParams(formdata);
                const response = await fetch('/filebrowser/delete', {
                    method: 'POST',
                    headers: {'Content-type': 'application/x-www-form-urlencoded'},
                    body: data
                });

                if (response.ok) {
                    let data = await response.json();
                    this.renderDirsAndFiles()
                    .then(response => {
                        console.log(response);
                        document.querySelector('.fb-actions').insertAdjacentHTML('afterend', `<div class="alert ${data.alert}"><strong>${data.alert}!</strong> ${data.message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>`);
                        document.querySelector('#check-all').checked = false;
                    })
                    .catch(error => console.log(error));
                    
                }
            }
        }); 
    }
    
    /**
     * Refresh filebrowser
     * 
     * @returns {void}
     */
    refresh() {
        const button = document.querySelector('.fb-btn-refresh');
        button.addEventListener('click', async () => {
            button.disabled = true;
            this.renderDirsAndFiles();
            button.disabled = false;
        });
    }

    /**
     * Open and close the upload panel
     * 
     * Open and close the upload panel but also add and remove certain elements
     * that could cause issue during the upload process.
     * 
     * @returns {void}
     */
    toggleUploadPanel() {
        document.querySelector('.fb-btn-upload-open').addEventListener('click', () => {
            document.querySelector('.fb-upload').classList.add('open');
            document.querySelector('.fb-btn-upload-close').style.display = 'inline-block';
            document.querySelector('.fb-btn-refresh').style.display = 'none';
            document.querySelector('.fb-btn-back').style.display = 'none';
        });
        document.querySelector('.fb-btn-upload-close').addEventListener('click', () => {
            document.querySelector('.fb-upload').classList.remove('open');
            document.querySelector('.fb-files').value = '';
            if (document.querySelector('.fb-preview li')) document.querySelector('.fb-preview').innerHTML = '';
            document.querySelector('.fb-btn-upload-close').style.display = 'none';
            document.querySelector('.fb-btn-refresh').style.display = 'inline-block';
            if (this.settings.dir != this.initial_dir) document.querySelector('.fb-btn-back').style.display = 'inline-block';
            this.items = [];
            this.cancel = true;
        });
    }
    
    upload() {
        this.captureUploadItems();
        this.cancelUpload();
        this.commitUpload();
        this.checkAll();
    }

    captureUploadItems() {
        const input = document.querySelector('.fb-upload-form input');
        let num = 1;
        input.addEventListener('change', () => {
            this.cancel = false;
            for (let i = 0; i < input.files.length; i++) {
                let re = /(?:\.([^.]+))?$/;
                let ext = re.exec(input.files[i].name)[1];
                if (ext && ext != 'undefined') {
                    input.files[i].id = num++;
                    this.items.push(input.files[i]);   
                } else {
                    alert(`It appears you tried to upload a folder (${input.files[i].name}). It was ignored because folders cannot be uploaded.`);
                }
                           
            }
            this.renderCapturedItems(this.items);
        });
    }

    renderCapturedItems(items) {
        items.forEach(v => {          
            const preview = document.querySelector('.fb-preview');
            if (!document.querySelector(`#item-${v.id}`)) {
                switch (v.type) {
                    case 'application/msword':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><i class="fas fa-file-word fa-fw"></i><span>${v.name}</span></li>`);
                        document.querySelectorAll('.fa-file-word').forEach(v => v.style.color = '#4472C4');
                    break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><i class="fas fa-file-excel fa-fw"></i><span>${v.name}</span></li>`);
                        document.querySelectorAll('.fa-file-excel').forEach(v => v.style.color = '#1D6F42');
                    break;
                    case 'application/pdf':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><embed src="${URL.createObjectURL(v)}"></i><span>${v.name}</span></li>`);
                        document.querySelectorAll('embed').forEach(v => v.style.marginLeft = '-15%');
                    break;
                    case 'application/x-zip-compressed':
                    case 'application/x-tar':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><i class="fas fa-file-archive fa-fw"></i><span>${v.name}</span></li>`);
                        document.querySelectorAll('.fa-file-archive').forEach(v => v.style.color = '#FFCC00');
                    break;
                    case 'application/vnd.ms-powerpoint':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><i class="fas fa-file-powerpoint fa-fw"></i><span>${v.name}</span></li>`);
                        document.querySelectorAll('.fa-file-powerpoint').forEach(v => v.style.color = '#DC3E15');
                    break;
                    case 'image/jpeg':
                    case 'image/tiff':
                    case 'image/gif':
                    case 'image/png':
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><img src="${URL.createObjectURL(v)}"></i><span>${v.name}</span></li>`);
                    break;
                    default:
                        preview.insertAdjacentHTML('beforeend', `<li id="item-${v.id}"><i class="fas fa-question-circle fa-fw"></i><span>${v.name}</span></li>`);
                    break;
                }
            }

            let previews = document.querySelectorAll('.fb-preview li'); 
            if (previews.length > 10) previews.forEach(e => e.style.height = '50%');
            if (previews.length > 20) previews.forEach(e => e.style.height = '30%');
        });
    }

    cancelUpload() {
        document.querySelector('.fb-cancel-upload').addEventListener('click', () => {
            document.querySelector('.fb-files').value = '';
            document.querySelector('.fb-preview').innerHTML = '';
            this.items = [];
            this.cancel = true;
        });
    }

    commitUpload() {
        const button = document.querySelector('.fb-commit-upload');
        button.addEventListener('click', async () => {
            button.disabled = true;
            if (document.querySelector('.fb-preview em')) document.querySelectorAll('.fb-preview em').forEach(v => v.remove());
            if (this.items.length > 0) {
                for (const v of this.items) {
                    if (this.cancel) break;
                    let li = document.querySelector(`#item-${v.id}`);
                    li.insertAdjacentHTML('beforeend', '<em><i class="fas fa-spinner fa-fw fa-spin"></i></em>');
                    let formdata = new FormData();
                    formdata.append('upload_item', v);
                    formdata.append('upload_dir', this.settings.dir);
                    const response = await fetch('/filebrowser/upload', {
                        method: 'POST',
                        body: formdata
                    });

                    if (response.ok) {
                        let data = await response.json();
                        if (data.alert == 'success') { 
                            li.querySelector('em').innerHTML = '<i class="fas fa-check fa-fw"></i>';
                            li.querySelector('em').style.color = 'green';
                        }
                        if (data.alert == 'error') { 
                            li.querySelector('em').innerHTML = `<i class="fas fa-times-circle fa-fw">${data.message}</i>`;
                            li.querySelector('em').style.color = 'red';
                            li.querySelector('em').setAttribute('title', data.message);
                        }
                    }
                }
                this.renderDirsAndFiles();
            } else {
                alert('You have not selected any items to upload.');
            }
            button.disabled = false;
        });
    }

    checkAll() {
        document.addEventListener('click', (e) => {
            if (e.target.hasAttribute('id') && e.target.id == 'check-all') {
                const checkboxes = document.querySelectorAll('.checkbox');
                if (e.target.checked) checkboxes.forEach(element => element.checked = true);
                else checkboxes.forEach(element => element.checked = false);
            }
        });
    }

    /**
     * Extend
     * 
     * Take this classes default parameters and overwrite and add to them with
     * settings from the user.
     * 
     * @param {object} defaults - JSON object of default settings
     * @param {object} settings - JSON object of custom settings
     * @returns {object}
     */
    extend(defaults, settings) {
        for (let prop in settings) {
            if (typeof settings[prop] === 'object') {
                defaults[prop] = this.extend(defaults[prop], settings[prop]);
            } else {
                defaults[prop] = settings[prop];
            }
        }
        return defaults;
    }
}

const filebrowser = new Filebrowser();