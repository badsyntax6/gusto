class Search {
    constructor() {
        // console.log('Search Started');
        this.init();
    }

    init() {
        const input = document.querySelector('.search-input');
        const results = document.querySelector('.search-results');
        if (!input) return false;
        input.addEventListener('keyup', () => {
            if (input.value.length > 1) {
                results.style.display = 'block';
                results.insertAdjacentHTML('beforeEnd', '<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
                fetch('/search', {
                    method: 'POST',
                    headers: {'Content-type': 'application/x-www-form-urlencoded'},
                    body: new URLSearchParams({'string' : input.value})
                })
                .then(response => response.json())
                .then(data => {
                    const loader = document.querySelector('.loading');
                    const ul = document.querySelector('.search-results ul');
                    if (loader) loader.remove();
                    if (!data) return false;
                    if (ul) ul.remove();
                    Object.entries(data).forEach(v => {
                        const title = v[0];
                        const data = v[1];
                        results.innerHTML = `<ul class="search-results-${title}"><em>${ucfirst(title)}:</em>`;
                        Object.values(data).forEach(v => {
                            document.querySelector(`.search-results-${title}`)
                            .innerHTML += `<li>
                                                <a href="/users/user/${v.firstname.toLowerCase()} - ${v.lastname.toLowerCase()}">${v.firstname} ${v.lastname} (${v.email})</a>
                                            </li>`;
                        });
                        results.innerHTML += '</ul>';
                    });
                });   
            } else {
                const ul = document.querySelector('.search-results ul');
                if (ul) ul.remove();
                results.style.display = 'none';             
            }
        });
    }
}

const search = new Search();