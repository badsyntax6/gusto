class PercentBar {
    constructor() {
        // console.log('Percent Bar Started');
    }

    async init(settings) {
        this.settings = this.extend({
            container: '',
            url: '',
            json: {},
            vertical: false
        }, settings);

        this.container = document.querySelector(this.settings.container);
        
        if (settings.vertical) this.container.classList.add('pb-vertical');
        else this.container.classList.add('pb-horizontal');
        
        try {
            const data = await this.get();
            return this.showPercent(data);
        } catch (error) {
            return error;
        }
    }

    async get() {
        const response = await fetch(this.settings.url);
        let data = null;

        if (this.settings.url !== '') {
            if (response.ok) data = await response.json();
            else return response;
        }
        
        if (data) return Object.assign({}, data, this.settings.json); // Combine url data object with json data object
        else return this.settings.json;
    }

    async showPercent(data) {
        const bars = this.container.querySelectorAll('.percent-bar');
        const s = this.settings
        bars.forEach(function(v) {
            const span = v.querySelector('span');
            if (data.hasOwnProperty(span.dataset.type)) {
                const num = parseInt(data[span.dataset.type]);
                const total = parseInt(data.total);
                const percent = Math.floor((num / total) * 100);
                if (s.vertical) { 
                    span.style.height = percent + '%';
                } else {
                    span.animate([{width: '0%'}, {width: `${percent}%`}], {duration: 700, fill: 'forwards', easing: 'ease-out'});
                }
                v.insertAdjacentHTML('beforeEnd', '<em>' + percent + '%' + '</em>');
            }
        });

        return data;
    }

    /**
     * Extend
     * 
     * Take this classes default parameters and overwrite and add to them with
     * settings from the user.
     * @param {object} defaults - JSON object of default settings
     * @param {object} settings - JSON object of custom settings
     * @return {object}
     */
    extend(defaults, settings) {
        for (let prop in settings) {
            if (typeof settings[prop] === 'object') {
                defaults[prop] = this.extend(defaults[prop], settings[prop]);
            } else {
                defaults[prop] = settings[prop];
            }
        }
        return defaults;
    }

}

const pbar = new PercentBar();