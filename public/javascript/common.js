

function on(type, e_el, callback) {
    const els = document.querySelectorAll(e_el);
    els.forEach(e => {
        e.addEventListener(type, () => {
            callback(e);
        });
    });
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function sort(data, dir = 'asc') {
    let arr = [];
    for(a in data) arr.push([a, data[a]]);
    arr.sort((a, b) => a[1] - b[1]);
    if (dir == 'desc') arr.reverse();
    var string = '{';
    arr.forEach((v, i) => string += '"' + v[0] + '": ' + v[1] + ', ');
    string = removeLastComma(string);
    string += '}';
    return JSON.parse(string);
}

function removeLastComma(strng) {        
    var n = strng.lastIndexOf(",");
    var a = strng.substring(0, n) 
    return a;
}

/** 
 * Serialize all form data into an array of key/value pairs
 * (c) 2020 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node} form The form to serialize
 * @return {Array} The serialized form data
 */
function serializeArray(form) {
   var arr = [];
   Array.prototype.slice.call(form.elements).forEach(function (field) {
       if (!field.name || field.disabled || ['file', 'reset', 'submit', 'button'].indexOf(field.type) > -1) return;
       if (field.type === 'select-multiple') {
           Array.prototype.slice.call(field.options).forEach(function (option) {
               if (!option.selected) return;
               arr.push({
                   name: field.name,
                   value: option.value
               });
           });
           return;
       }
       if (['checkbox', 'radio'].indexOf(field.type) >-1 && !field.checked) return;
       arr.push({
           name: field.name,
           value: field.value
       });
   });
   return arr;
};

/**
 * Extend
 * 
 * Take this classes default parameters and overwrite and add to them with
 * settings from the user.
 * 
 * @param {object} defaults - JSON object of default settings
 * @param {object} settings - JSON object of custom settings
 * @return {object}
 */
function extend(defaults, settings) {
    for (let prop in settings) {
        if (typeof settings[prop] === 'object') {
            defaults[prop] = this.extend(defaults[prop], settings[prop]);
        } else {
            defaults[prop] = settings[prop];
        }
    }
    return defaults;
}

// https://stackoverflow.com/questions/32589197/how-can-i-capitalize-the-first-letter-of-each-word-in-a-string-using-javascript
function titleCase(str) {
    let split_string = str.toLowerCase().split(' ');
    for (let i = 0; i < split_string.length; i++) {
        split_string[i] = split_string[i].charAt(0).toUpperCase() + split_string[i].substring(1);     
    }
    return split_string.join(' '); 
 }