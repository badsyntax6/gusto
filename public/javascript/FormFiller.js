class FormFiller {

    constructor() {
        // console.log('FormFiller Started');
    }

    async init(settings) {
        this.settings = extend({
            form: 'form',
            url: 'formfiller.php',
            json: {},
            ignore: []
        }, settings);
        
        try {
            return this.fillForm();
        }
        catch (error) {
            return error;
        }
    }
  
    async get() {
        const response = await fetch(this.settings.url);
        let data = null;

        if (response.ok) data = await response.json();
        else return response;
        if (data) return Object.assign({}, data, this.settings.json); // Combine url data object with settings json data object
        else return this.settings.json;   
    }

    async fillForm() {
        const data = await this.get();
        const {ignore, form} = this.settings;
        const inputs = document.querySelectorAll(`${form} input, ${form} textarea, ${form} select`);
        
        // Check if there is any data to fill the form with
        if (Object.keys(data).length === 0) {
            return { 
                status: 'Error',
                message: 'Form Filler did not recieve any form data. Be sure to provide the config with json and/or a url',
                target: form
            }
        }
        
        // Remove ignored properties from data object
        ignore.forEach(v => delete data[v]);
        
        // Fill in the form
        inputs.forEach(input => {
            for (let key in data) {
                if (key == input.name && data[key] != null) {
                    switch (input.type) {
                        case 'checkbox':
                            if (data[key] === true) input.checked = true;
                        break;
                        case 'radio':
                            let radio = document.querySelectorAll(form + ' input[name="' + key + '"]');
                            radio.forEach((input) => { 
                                if (input.value == data[key]) input.checked = true;
                            });
                        break;  
                        case 'select-one':
                        case 'textarea':
                        case 'text':
                        case 'tel':
                        case 'date':
                        case 'time':
                        case 'email':
                        case 'hidden':
                        case 'password': 
                        case 'color':
                            input.value = data[key];
                        break;
                    }
                    delete data[key];
                }
            }
        });

        // Create the unfilled array
        let unfilled = [];
        inputs.forEach(v => {
            switch (v.type) {
                case 'radio': 
                    if (v.checked === false && !unfilled.includes(v.name)) unfilled.push(v.name);
                break;
                case 'checkbox': if (v.checked === false) unfilled.push(v.name); break;
                default: if (!v.value) unfilled.push(v.name); break;
            }
        });

        return {
            status: 'Success',
            message: 'Form Filler completed successfuly',
            target: form,
            leftover: data,
            unfilled: unfilled,
            ignored: ignore
        }
    }
}

function extend(defaults, settings) {
    for (let prop in settings) {
        if (typeof settings[prop] === 'object') {
            defaults[prop] = this.extend(defaults[prop], settings[prop]);
        } else {
            defaults[prop] = settings[prop];
        }
    }
    return defaults;
}

const formfiller = new FormFiller();