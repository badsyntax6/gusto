class HTTP {
    constructor() {
        this.error = false;
        console.log('HTTP Started');
    }

    static async get(path, callback) {
        const parent = this;
        const data = await fetch(path).then(response => {
            if (response.status == 404) parent.error = 404;
            return response.json();
        }).catch(error => {
            if (parent.error == 404) return `404 Error: The file or url "${path}" cannot be found.`;
            return error;
        });

        callback(data);
    }

    static async post(path, data, callback) {
        const parent = this;
        let form_data = null;

        if (typeof data == 'string') {
            const form_el = document.querySelector(data);
            form_data = new URLSearchParams(new FormData(form_el)).toString();
        }
        if (typeof data == 'object') form_data = new URLSearchParams(data);
        // console.log(form_data);
        const response = await fetch(path, {
            method: 'POST',
            body: form_data
        }).then(response => {
            if (response.status == 404) parent.error = 404;
            return response.json();
        }).catch(error => {
            if (parent.error == 404) return `404 Error: The file or url "${path}" cannot be found.`;
            return error;
        });

        callback(response);
    }

    static serialize(form) {

        // Setup our serialized data
        var serialized = [];

        // Loop through each field in the form
        for (var i = 0; i < form.elements.length; i++) {

            var field = form.elements[i];

            // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
            if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

            // If a multi-select, get all selections
            if (field.type === 'select-multiple') {
                for (var n = 0; n < field.options.length; n++) {
                    if (!field.options[n].selected) continue;
                    serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
                }
            }

            // Convert field data to a query string
            else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
            }
        }

        return serialized.join('&');
    }
}