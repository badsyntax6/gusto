
/**
 * Calendar
 *
 * The Calendar takes json data from the calendar controller and 
 * displays it on the page
 *
 * Example
 *      let calendar = new Calendar();
 *      calendar.init({
 *          view: 'month',
 *          events: '/calendar/get-events'
 *      }).then(response => console.log(response));
 * 
 * SEE: Calendar Controller Class - /root/private/controllers/CalendarController.php
 */

class Calendar {
    constructor() {
        // console.log('Calendar Instantiated');
    }

    init(settings) {
        this.settings = extend({
            table: '#calendar-table',  // Table element to fill with calendar. DEFAULT: #calendar-table
            date: '',          // Display a specific calendar time period based on this date. DEFAULT: ''
            view: 'month',       // day | week | month. DEFAULT: month
            events_url: null,          // URL to get calendar json data from. DEFAULT: null
            day_click: true,     // When in month view make the day button clickable to jump to that day. DEFAULT: true
            allow_nav: true,    // Allow the user to go forwards and backwards on the calendar. DEFAULT: true
            log: false
        }, settings);

        this.table = document.querySelector(this.settings.table);
        this.prev = null;
        this.next = null;
        this.events = null;
        this.initial_render = true;
        this.listen();

        try {
            return this.render();
        } catch (error) {
            return error;
        }
    }

    render(date = '') {
        let view = this.settings.view;
        if (this.initial_render) this.initial_render = false;
        else view = document.querySelector('#calendar-table').getAttribute('data-view');
        if (view == 'month') return this.drawMonthView(date);
        if (view == 'week') return this.drawWeekView(date);
        if (view == 'day') return this.drawDayView(date);
    }

    async getCalendar(date = '') {
        const post = await fetch('/calendar/getCalendar', { method: 'POST', body: new URLSearchParams({ date: date }) });
        if (post.ok) return await post.json();
    }

    async getEvents(date = '') {
        const post = await fetch(this.settings.events_url, { method: 'POST', body: new URLSearchParams({ date: date }) });
        if (post.ok) return await post.json();
    }

    async getEvent(id) {
        const get = await fetch(`/calendar/getEventJson/${id}`);
        if (get.ok) return await get.json();
    }

    async saveNewEvent() {
        const form = document.querySelector('#calendar-form');
        const post = await fetch('/calendar/save', { method: 'POST', body: new FormData(form) });
        if (post.ok) return await post.json();
    }

    async updateEvent() {
        const form = document.querySelector('#calendar-form');
        const post = await fetch('/calendar/update', { method: 'POST', body: new FormData(form) });
        if (post.ok) return await post.json();
    }

    async deleteEvent(id) {
        const post = await fetch('/calendar/delete', { method: 'POST', body: new URLSearchParams({ id: id }) });
        if (post.ok) return await post.json();
    }

    reset(title) {
        if (document.querySelector('.this-month')) document.querySelector('.this-month').innerText = title;
        if (document.querySelector('#calendar-table tr')) document.querySelectorAll('#calendar-table tr').forEach(e => e.remove());
        if (document.querySelector('#calendar-table tbody')) document.querySelectorAll('#calendar-table tbody').forEach(e => e.remove());
    }

    listen() {
        document.addEventListener('click', async (e) => {
            const t = e.target;
            const p = t.parentElement;
            if (t.classList.contains('btn-prev') || p.classList.contains('btn-prev')) {
                this.render(this.prev);
            }
            if (t.classList.contains('btn-next') || p.classList.contains('btn-next')) {
                this.render(this.next);
            }
            if (t.classList.contains('btn-today') || p.classList.contains('btn-today')) {
                if (this.table.hasAttribute('data-view')) {
                    if (this.table.getAttribute('data-view') == 'month') this.drawMonthView();
                    if (this.table.getAttribute('data-view') == 'week') this.drawWeekView();
                    if (this.table.getAttribute('data-view') == 'day') this.drawDayView();
                }
            }
            if (t.classList.contains('btn-month') || p.classList.contains('btn-month')) this.drawMonthView(this.calendar.date);
            if (t.classList.contains('btn-week') || p.classList.contains('btn-week')) this.drawWeekView(this.calendar.date);
            if (t.classList.contains('btn-day') || p.classList.contains('btn-day')) this.drawDayView(this.calendar.date);
            if (t.classList.contains('btn-cal-day')) {
                if (this.settings.day_click) this.drawDayView(`${t.innerText} ${this.calendar.month_year}`);
            }
            if (this.table.getAttribute('data-view') == 'week') {
                if (t.nodeName == 'TH') {
                    this.drawDayView(t.getAttribute('data-week'));
                }
            }

            if (t.classList.contains('event')) this.enlargeEvent(t);
            if (p.classList.contains('event')) this.enlargeEvent(p);

            if (t.classList.contains('btn-new-event') || p.classList.contains('btn-new-event')) {
                document.querySelector('#calendar-form').classList.add('open');
                document.querySelector('#new-event-overlay').classList.add('open');
                document.querySelector('#new-event-overlay').style.transform = 'scale(1)';
            }

            if (t.classList.contains('cm')) {
                let large_event = document.querySelectorAll('.event-large');
                if (large_event.length <= 0) {
                    document.querySelector('#calendar-form').classList.add('open');
                    document.querySelector('#new-event-overlay').classList.add('open');
                    document.querySelector('#new-event-overlay').style.transform = 'scale(1)';
                    let date = parseInt(t.getAttribute('data-date'));
                    date = new Date(date).toISOString();
                    date = date.substr(0, date.lastIndexOf("."));
                    document.querySelector('[name="start_date"]').value = date;
                }
            }

            let large_event = document.querySelectorAll('.event-large');
            if (large_event && large_event.length > 0) large_event.forEach(e => e.remove());

            if (t.classList.contains('btn-save') || p.classList.contains('btn-save')) {
                const response = await this.saveNewEvent();
                if (response) {
                    this.render();
                    document.querySelector('#calendar-form').classList.remove('open');
                    document.querySelector('#new-event-overlay').classList.remove('open');
                    document.querySelector('#new-event-overlay').style.transform = 'scale(0)';
                    document.querySelector('#calendar-form').reset();
                    document.querySelector('.calendar-form-controls').insertAdjacentHTML('afterbegin', `<div class="alert ${response.alert} row text-left mb15"><strong>${response.alert}!</strong> ${response.message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>`);
                    document.querySelector('.btn-event').classList.remove('btn-update');
                    document.querySelector('.btn-event').classList.add('btn-save');
                    document.querySelector('.btn-event').innerHTML = '<i class="fas fa-save fa-fw"></i> Save Event';
                }
            }

            if (t.classList.contains('btn-edit-event') || p.classList.contains('btn-edit-event')) {
                document.querySelector('#calendar-form').classList.add('open');
                document.querySelector('#new-event-overlay').classList.add('open');
                const id = t.getAttribute('data-id');
                const event = await this.getEvent(id);
                const {event_id, title, description, start_time, end_time, start_date, end_date, color} = event;
                document.querySelector('[name="title"]').value = title;
                document.querySelector('[name="start_date"]').value = `${start_date}T${start_time}`;
                document.querySelector('[name="end_date"]').value = `${end_date}T${end_time}`;
                document.querySelector('[name="description"]').value = description;
                document.querySelectorAll('[name="color"]').forEach(v => {
                    if (v.value == color) v.checked = true;
                });
                document.querySelector('#calendar-form').insertAdjacentHTML('beforeend', `<input name="event_id" type="hidden" value="${event_id}">`);
                document.querySelector('.btn-event').classList.add('btn-update');
                document.querySelector('.btn-event').classList.remove('btn-save');
                document.querySelector('.btn-event').innerHTML = '<i class="fas fa-save fa-fw"></i> Update Event';
            }

            if (t.classList.contains('btn-update') || p.classList.contains('btn-update')) {
                const response = await this.updateEvent();
                if (response) {
                    this.render();
                    document.querySelector('#calendar-form').classList.remove('open');
                    document.querySelector('#new-event-overlay').classList.remove('open');
                    document.querySelector('#new-event-overlay').style.transform = 'scale(0)';
                    document.querySelector('#calendar-form').reset();
                    document.querySelector('.calendar-form-controls').insertAdjacentHTML('afterbegin', `<div class="alert ${response.alert} row text-left mb15"><strong>${response.alert}!</strong> ${response.message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>`);
                }
            }

            if (t.classList.contains('btn-del-event') || p.classList.contains('btn-del-event')) {
                if (confirm('Are you sure you want to delete this event?')) {
                    const response = await this.deleteEvent(t.getAttribute('data-id'));
                    if (response && response.alert == 'success') {
                        this.render();
                    } else {
                        document.querySelector('#calendar-table').insertAdjacentHTML('beforebegin', `<div class="alert ${response.alert} row text-left mb15"><strong>${response.alert}!</strong> ${response.message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>`);
                    }
                }
            }

            if (t.classList.contains('btn-event-close') || p.classList.contains('btn-event-close')) {
                document.querySelector('#calendar-form').classList.remove('open');
                document.querySelector('#new-event-overlay').classList.remove('open');
                setTimeout(() => {
                    document.querySelector('#new-event-overlay').style.transform = 'scale(0)';
                }, 500);
            }
        });
    }

    async drawMonthView(date = this.settings.date) {
        const calendar = await this.getCalendar(date);
        let month_view = '<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>';

        this.reset(`${calendar.month} ${calendar.year}`);
        this.table.setAttribute('data-view', 'month');
        this.prev = calendar.last_month;
        this.next = calendar.next_month;
        this.calendar = calendar;

        let week = '';
        let lm_end = parseInt(calendar.lm_day_count);
        let lm_start = lm_end - calendar.dow + 1;
        let todays_date = Date.parse(`${calendar.todays_date_iso} 00:00:00`);
        
        for (let day = lm_start; day <= lm_end; day++) week += `<td class="om">${day}</td>`;
        for (let day = 1; day <= calendar.day_count; day++, calendar.dow++) {
            let date = Date.parse(`${calendar.year}-${calendar.month_num}-${day} 00:00:00`);
            week += `<td data-date="${date}" class="cm"><button type="button" class="btn btn-cal-day no-load">${day}</button><div class="events"></div></td>`;
            if (calendar.dow % 7 == 6 || day == calendar.day_count) {
                if (day == calendar.day_count) {
                    let cells = 6 - (calendar.dow % 7);
                    for (let num = 1; num <= cells; num++) week += `<td class="nm">${num}</td>`;
                }
                month_view += `<tr class="calendar-week">${week}</tr>`
                week = '';
            }
        }

        this.table.insertAdjacentHTML('afterbegin', month_view);
        await this.renderEvents(this.calendar.date);

        let today_data = document.querySelector(`[data-date="${todays_date}"]`);
        if (today_data) today_data.classList.add('today');

        if (this.settings.log) console.log({calendar: calendar, events: this.events, settings: this.settings});

        return 'Initial render is month view';
    }

    async drawWeekView(date = this.settings.date) {
        const calendar = await this.getCalendar(date);
        let week_view = '';
        let week = [];
        let lm_end = parseInt(calendar.lm_day_count);
        let lm_start = lm_end - calendar.dow + 1;
        let tm = calendar.month_year;
        let lm = calendar.last_month;
        let nm = calendar.next_month;

        this.table.setAttribute('data-view', 'week');
        this.calendar = calendar;

        for (let day = lm_start; day <= lm_end; day++) week.push(`${day} ${lm}`);
        for (let day = 1; day <= calendar.day_count; day++, calendar.dow++) {
            week.push(`${day} ${tm}`);
            if (calendar.dow % 7 == 6 || day == calendar.day_count) {
                if (day == calendar.day_count) {
                    let cells = 6 - (calendar.dow % 7);
                    for (let num = 1; num <= cells; num++) week.push(`${num} ${nm}`);
                }
            }
        }

        let weeks = [];
        let w1 = week.slice(0, 7), w2 = week.slice(7, 14), w3 = week.slice(14, 21), w4 = week.slice(21, 28), w5 = week.slice(28, 35), w6 = week.slice(35, 42);
        let week_start;
        let week_end;
        let w;

        weeks.push(w1, w2, w3, w4, w5, w6);
        weeks.forEach(v => {
            if (!w) {
                if (v.includes(`${calendar.day} ${tm}`)) w = v;
            }
        });

        let lw = parseInt(w[0].split(' ')[0]);
        let lwm = `${w[0].split(' ')[1]} ${w[0].split(' ')[2]}`;
        let nw = parseInt(w[6].split(' ')[0]);
        let nwm = `${w[6].split(' ')[1]} ${w[6].split(' ')[2]}`;

        this.prev = lw - 1 + ' ' + tm;
        if (lw == 1) this.prev = `${calendar.lm_day_count} ${calendar.last_month}`;
        if (lwm == lm) this.prev = lw - 1 + ' ' + calendar.last_month;

        this.next = nw + 1 + ' ' + tm;
        if (nw == 31) this.next = 1 + ' ' + calendar.next_month;
        if (nwm == nm) this.next = parseInt(w[6].split(' ')[0]) + 1 + ' ' + calendar.next_month;

        week_start = w[0];
        week_end = w[6];

        if (week_start.split(' ')[1] == week_end.split(' ')[1]) week_start = week_start.split(' ')[0];
        else week_start = `${week_start.split(' ')[0]} ${week_start.split(' ')[1]}`;

        this.reset(week_start + ' - ' + week_end);

        week_view += `<tr>
                        <th></th>
                        <th data-week="${w[0]}">Sun ${w[0].split(' ')[0]}</th>
                        <th data-week="${w[1]}">Mon ${w[1].split(' ')[0]}</th>
                        <th data-week="${w[2]}">Tue ${w[2].split(' ')[0]}</th>
                        <th data-week="${w[3]}">Wed ${w[3].split(' ')[0]}</th>
                        <th data-week="${w[4]}">Thu ${w[4].split(' ')[0]}</th>
                        <th data-week="${w[5]}">Fri ${w[5].split(' ')[0]}</th>
                        <th data-week="${w[6]}">Sat ${w[6].split(' ')[0]}</th>
                    </tr>`;

        let time = 0;
        for (let hour = 0; hour <= 11; hour++, time++) {
            week_view += `<tr><td class="time-td">${hour}am</td>`;
            w.forEach(v => week_view += `<td data-day="${Date.parse(new Date(v))}" data-time="${time}"><div class="events"></div></td>`);
            week_view += '</tr>';
        }

        time = 12;
        for (let hour = 0; hour <= 11; hour++, time++) {
            week_view += `<tr><td class="time-td">${hour}pm</td>`;
            w.forEach(v => week_view += `<td data-day="${Date.parse(new Date(v))}" data-time="${time}"><div class="events"></div></td>`);
            week_view += '</tr>';
        }

        this.table.insertAdjacentHTML('afterbegin', week_view);

        document.querySelectorAll('td:first-child').forEach(v => {
            if (v.innerText == '0am') v.innerText = '12am';
            if (v.innerText == '0pm') v.innerText = '12pm';
        });

        await this.renderEvents(this.calendar.date);
        
        if (this.settings.log) console.log({calendar: calendar, events: this.events, settings: this.settings});

        return 'Initial render is week view';
    }

    async drawDayView(date = this.settings.date) {
        const calendar = await this.getCalendar(date);
        let yesterday = parseInt(calendar.day) - 1;
        let tomorrow = parseInt(calendar.day) + 1;
        let my = `${calendar.month} ${calendar.year}`;
        let lm = calendar.last_month;
        let nm = calendar.next_month;
        let day_count = parseInt(calendar.day_count);
        let day_view = '';

        this.prev = `${yesterday} ${my}`;
        this.next = `${tomorrow} ${my}`;
        this.calendar = calendar;

        if (yesterday == 0) {
            yesterday = calendar.lm_day_count;
            this.prev = `${yesterday} ${lm}`;
        }

        if (tomorrow > day_count) {
            tomorrow = 1;
            this.next = `${tomorrow} ${nm}`;
        }

        this.reset(`${calendar.day} ${calendar.month} ${calendar.year}`);
        this.table.setAttribute('data-view', 'day');

        day_view += `<th></th><th colspan="8">${calendar.today}</th>`;
        day_view += '<tr><td class="time-td">12am</td><td data-time="0"><div class="events"></div></td></tr>';

        let time = 1;
        for (let hour = 1; hour <= 11; hour++, time++) {
            day_view += `<tr><td class="time-td">${hour}am</td><td data-time="${time}"><div class="events"></div></td></tr>`;
        }
        day_view += '<tr><td class="time-td">12pm</td><td data-time="12"><div class="events"></div></td></tr>';
        time = 13;
        for (let hour = 1; hour <= 11; hour++, time++) {
            day_view += `<tr><td class="time-td">${hour}pm</td><td data-time="${time}"><div class="events"></div></td></tr>`;
        }

        this.table.insertAdjacentHTML('afterbegin', day_view);
        await this.renderEvents(this.calendar.date);
        
        if (this.settings.log) console.log({calendar: calendar, events: this.events, settings: this.settings});

        return 'Initial render is day view';
    }

    async renderEvents(date) {
        const data = await this.getEvents(date);
        const events = data.data;
        const view = this.table.getAttribute('data-view');
        this.events = events;
        if (!events) return false;
        document.querySelectorAll('td').forEach(v => {
            events.forEach(e => {
                let border = adjustCardColors(e.color);
                if (view == 'month' && v.classList.contains('cm')) {
                    const this_day = v.getAttribute('data-date');
                    const event_start = Date.parse(`${e.start_month_iso}-${e.start_day} 00:00:00`);
                    const event_end = Date.parse(`${e.end_month_iso}-${e.end_day} 00:00:00`);
                    const event = `<div class="event" data-id="${e.id}" style="background-color:${e.color}; border-left:5px solid ${border};">
                                <p>${e.title} - ${e.description} ${e.start_time_short} 
                                    <span>${e.start_day}/${e.start_month.replace(' ', '/')} ${e.start_time} - ${e.end_day}/${e.end_month.replace(' ', '/')} ${e.end_time}</span>
                                </p>
                            </div>`;
                    if (event_start <= this_day && this_day <= event_end) {
                        v.querySelector('.events').insertAdjacentHTML('beforeend', event);
                    }
                }
                if (view == 'week' && !v.classList.contains('time-td')) {
                    const week_day = parseInt(v.getAttribute('data-day'));
                    const time = parseInt(v.getAttribute('data-time'));
                    let start_day = Date.parse(`${e.start_month_iso}-${e.start_day} 00:00:00`);
                    const end_day = Date.parse(`${e.end_month_iso}-${e.end_day} 00:00:00`);
                    const start_time = parseInt(e.start_time_mil);
                    const end_time = parseInt(e.end_time_mil);
                    const event = `<div class="event" data-id="${e.id}" style="background-color:${e.color}; border-left:3px solid ${border};">
                                <p>${e.title} - ${e.description} ${e.start_time_short} <span>${e.start_day}/${e.start_month.replace('-', '/')} ${e.start_time} - ${e.end_time}</span></p>
                            </div>`;
                    
                    if (week_day >= start_day && week_day <= end_day) {
                        if (time >= start_time && time <= end_time) v.querySelector('.events').insertAdjacentHTML('beforeend', event);
                    }
                }
                if (view == 'day' && !v.classList.contains('time-td')) {
                    const time = parseInt(v.getAttribute('data-time'));
                    let start_day = parseInt(e.start_day);
                    const end_day = parseInt(e.end_day);
                    const start_time = parseInt(e.start_time_mil);
                    const end_time = parseInt(e.end_time_mil);
                    const start_month = Date.parse(e.start_month);
                    const last_month = Date.parse(e.last_month);
                    const today = parseInt(e.today);
                    const event = `<div class="event" data-id="${e.id}" style="background-color:${e.color}; border-left:3px solid ${border};">
                                <p>${e.title} - ${e.description} <strong>${e.start_time} - ${e.end_time}</strong></p>
                            </div>`;

                    if (start_month == last_month) start_day = 1;
                    console.log(today, start_day, end_day);
                    if (today >= start_day && today <= end_day) {
                        if (time >= start_time && time <= end_time) v.querySelector('.events').insertAdjacentHTML('beforeend', event);
                    }
                }
            });
        });
    }

    async enlargeEvent(t) {
        const large_events = document.querySelectorAll('.event-large');
        if (large_events) large_events.forEach(e => e.remove());  
        
        const id = t.getAttribute('data-id');
        const post = await fetch('/calendar/getEvent', { method: 'POST', body: new URLSearchParams({ id: id }) });
        let data = null;
        if (post.ok) data = await post.json();

        const event = `<div class="event-large">
                            <div class="event-large-header">
                                <i class="fas fa-calendar-alt fa-fw"></i> ${data.title} 
                                <button type="button" class="btn-del-event" data-id="${id}"><i class="far fa-trash-alt fa-fw"></i> Delete</button>
                                <button type="button" class="btn-edit-event" data-id="${id}"><i class="fas fa-pencil-alt fa-fw"></i> Edit</button>
                            </div>
                            <p>${data.description}<span><i class="far fa-clock fa-fw"></i> ${data.start_time} - ${data.end_time}</span></p>
                        </div>`;

        t.insertAdjacentHTML('afterbegin', event);        
        document.querySelector('.event-large-header').style = `${t.getAttribute('style')} border: none`;
    }
}

function adjustColor(col, amt) {
    let use_pound = false;

    if (col[0] == '#') {
        col = col.slice(1);
        use_pound = true;
    }

    let num = parseInt(col, 16);
    let r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    let b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    let g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (use_pound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
}

function colorIsDark(color) {
    let r, g, b, hsp;

    color = + ('0x' + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
    r = color >> 16;
    g = color >> 8 & 255;
    b = color & 255;

    hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));

    if (hsp > 127.5) return false;// console.log('light');
    else return true; // console.log('dark');
}

function adjustCardColors(bkg) {
    if (colorIsDark(bkg)) ammount = 70;
    else ammount = -70;
    return adjustColor(bkg, ammount);
}

function extend(defaults, settings) {
    for (let prop in settings) {
        if (typeof settings[prop] === 'object') {
            defaults[prop] = this.extend(defaults[prop], settings[prop]);
        } else {
            defaults[prop] = settings[prop];
        }
    }
    return defaults;
}

let calendar = new Calendar();