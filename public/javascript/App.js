class App {

    constructor() {
        // console.log('App Started');
        this.toggleDropdowns();
        this.highlightNavlink();
        this.goToTop();
        this.goBack();
        this.changeMenuSetting();
        this.adjustMainHeight();
        this.toggleFullscreen();
        this.handleAlerts();
        this.showButtonLoad();
        this.highlightTableRow();
        this.activityCheck();

        window.addEventListener('load', () => this.mobilizeTables());
        window.addEventListener('resize', () => this.mobilizeTables());
    }

    toggleDropdowns() {
        const dd_btns = document.querySelectorAll('.dropdown-button');
        dd_btns.forEach((element) => {
            element.addEventListener('click', () => {
                const dd_menu = element.querySelector('.dropdown-menu');
                const toggle = element.parentElement.querySelector('.dd-toggle');

                if (dd_menu.classList.contains('active')) {
                    dd_menu.classList.remove('active');
                    if (toggle) toggle.classList.remove('fa-flip-vertical');
                } else {
                    dd_menu.classList.add('active');
                    if (toggle) toggle.classList.add('fa-flip-vertical');
                }
            }); 
        });
    }

    highlightNavlink() {
        const path = window.location.pathname.split('/');
        let nav_class = path[1] ? '.nav-link.' + path[1] : '.nav-link.dashboard';
        if (path[1] == 'settings') nav_class = path[2] ? '.nav-link.' + path[2] : '';
        const nav_link = document.querySelector(nav_class);
        if (nav_link) nav_link.classList.add('current');        
        const current = document.querySelector('.dropdown-menu .current');
        if (current) {
            current.parentElement.parentElement.classList.add('active');
            current.parentElement.parentElement.previousElementSibling.querySelector('.dd-toggle').classList.add('fa-flip-vertical');
        }
    }

    goToTop() {
        document.body.insertAdjacentHTML('beforeEnd', '<div class="to-top"><i class="fas fa-angle-up"></i></div>');
        window.onscroll = (e) => {  
            const to_top = document.querySelector('.to-top');
            const main_nav = document.querySelector('.main-nav');
            if (window.pageYOffset > 520) to_top.classList.add('visible');
            else to_top.classList.remove('visible');
            if (main_nav && main_nav.classList.contains('wide')) {
                to_top.cssText = 'width: 40px; overflow: hidden; left: 10px';
                to_top.querySelector('i').cssText = 'width: 40px';
            } else {
                to_top.removeAttribute('style');
                to_top.querySelector('i').removeAttribute('style');
            }
            document.querySelector('.to-top').addEventListener('click', () => {
                window.scroll({top: 0, left: 0, behavior: 'smooth'}); // https://css-tricks.com/snippets/jquery/smooth-scrolling/
            });
        } 
    }

    showLoader(element) {
        document.querySelector(element).insertAdjacentHTML('beforeEnd', '<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
    }

    removeLoader(element) {
        const loaders = document.querySelector(element).querySelectorAll('.loading');
        loaders.forEach((v) => v.remove());
    }

    goBack() {
        let breadcrumb = window.location.pathname.split('/');
        breadcrumb.pop();
        if (breadcrumb && breadcrumb.length == 1) breadcrumb[0] = '/';
        const back = breadcrumb.join('/');
        const back_button = document.querySelector('.btn-goback');
        if (back_button) back_button.setAttribute('href', back);        
    }

    changeMenuSetting() {
        const main_nav = document.querySelector('.main-nav');
        const btn = document.querySelector('.btn-menu');

        if (window.outerWidth > 1024 && btn) {
            btn.addEventListener('click', () => {
                if (btn.parentElement.parentElement.parentElement.classList.contains('wide')) {
                    var menu_status = 0;
                } else {
                    var menu_status = 1;
                }
                   
                const data = new URLSearchParams({'menu_status' : menu_status});
                fetch('/settings/changeMenuSetting', {
                    method: 'POST',
                    // headers: {'Content-type': 'application/x-www-form-urlencoded'},
                    body: data
                }).then((response) => response.json()).then((data) => {
                    if (data == 0) {
                        document.querySelector('.btn-menu i').classList.remove('fa-toggle-on');
                        document.querySelector('.btn-menu i').classList.add('fa-toggle-off');
                        main_nav.classList.remove('wide');
                        document.querySelector('.logo').classList.add('logo-white');
                    } else {
                        document.querySelector('.btn-menu i').classList.remove('fa-toggle-off');
                        document.querySelector('.btn-menu i').classList.add('fa-toggle-on');
                        main_nav.classList.add('wide');
                        document.querySelector('.logo').classList.remove('logo-white');
                    }
                });                  
            });
        }

        if (main_nav) {
            if (main_nav.offsetWidth < 100) {
                document.querySelector('.logo').classList.add('logo-white');
            } else {
                document.querySelector('.logo').classList.remove('logo-white');
            }   
        }

    }

    openMobileMenu() {
        document.querySelector('.btn-mobile-menu').addEventListener('click', () => {
            if (this.classList.contains('open')) document.querySelector('.main-nav').classList.remove('open');
            else document.querySelector('.main-nav').classList.add('open');
        });
    }

    toggleFullscreen() {
        if (document.fullscreenEnabled) {
            const btn = document.querySelector('.btn-fullscreen');
            if (btn) {
                btn.addEventListener('click', () => {
                    if (document.fullscreenElement) document.exitFullscreen();
                    else document.documentElement.requestFullscreen();
                }, false);
            }
    
            document.addEventListener('fullscreenchange', () => {
                if (document.fullscreenElement) {
                    setTimeout(() => this.adjustMainHeight(), 100);
                    btn.style.color = '#aaa';
                } else {
                    btn.removeAttribute('style');
                }
            });
            
            document.addEventListener('fullscreenerror', () => console.log(event));
        }
    }

    adjustMainHeight() {
        let main = document.querySelector('main');
        main.style.cssText = 'min-height: '+ window.innerHeight +'px;';
    }

    handleAlerts() {
        document.addEventListener('click', (e) => {
            if (e.target.classList.contains('alert-redirect')) location.reload();
        });
        document.addEventListener('click', (e) => {
            if (e.target.parentElement) {
                if (e.target.parentElement.classList.contains('alert-close')) e.target.parentElement.parentElement.remove();
            }
        });        
    }

    showButtonLoad() {
        document.body.addEventListener('click', (e) => {
            if (e.target.classList.contains('btn')) {
                const btn = e.target;
                const alerts = document.querySelectorAll('.alert');
                const alert_areas = document.querySelectorAll('.alert-area');
                const html = btn.innerHTML;
                if (alerts) alerts.forEach(a => a.remove());
                if (alert_areas) alert_areas.forEach(aa => aa.innerHTML = '');
                if (btn.classList.contains('no-load')) return false; 
                btn.innerHTML = '<i class="fas fa-circle-notch fa-fw fa-spin"></i>'; 
                btn.setAttribute('disabled', true);
                setTimeout(() => {
                    btn.innerHTML = html;
                    btn.removeAttribute('disabled');
                }, 5000);
            }
        });
    }

    captureButton(el) {
        const btn = document.querySelector(el);
        if (btn) {
            const btn_html = btn.innerHTML;
            return {'btn': btn, 'html': btn_html};
        }
    }
    
    restoreButton(btn) {
        btn.btn.innerHTML = btn.html;
        btn.btn.disabled = false;
    }

    mobilizeTables() {
        if (window.innerWidth <= 1000) {
            let th = [];
            let num = 3;
            const rows = document.querySelectorAll('.data-row');
            document.querySelectorAll('th').forEach(element => th.push(element.innerText));
            rows.forEach(element => {
                element.querySelectorAll('.td').forEach(td => {
                    td.insertAdjacentHTML('afterBegin', '<span class="inline-label">' + th[num] + '</span>');
                    num++;
                });
                num = 3;
            });

        } else {
            const labels = document.querySelectorAll('.inline-label');
            if (labels.length) labels.forEach(l => l.remove());  
        }   
    }

    highlightTableRow() {
        document.body.addEventListener('click', (e) => {
            const target = e.target;
            const parent = target.parentElement;
            document.querySelectorAll('tr').forEach(el => el.removeAttribute('style'));
            if (target.nodeName == 'TR') {
                target.style.cssText = 'background-color: #f7f7f7; box-shadow: inset 0px 12px 3px -10px rgba(0, 0, 0, .1), inset 0px -11px 3px -10px rgba(0, 0, 0, .1); ';
            }
            if (parent.nodeName == 'TR') {
                parent.style.cssText = 'background-color: #f7f7f7; box-shadow: inset 0px 12px 3px -10px rgba(0, 0, 0, .1), inset 0px -11px 3px -10px rgba(0, 0, 0, .1); ';
            }
        });
    }

    activityCheck() {
        let idle_time = 0;
        document.addEventListener('DOMContentLoaded', () => {
            setInterval(async () => {
                const response = await fetch('/settings/activity-check'); 
                if (response.ok) {
                    const data = await response.json();
                    if (data !== 0) {
                        idle_time++;
                        if (idle_time > data) window.location.replace('/logout/inactive');
                    }
                }
            }, 60000);
            document.body.addEventListener('mousemove', () => idle_time = 0);
            document.body.addEventListener('keypress', () => idle_time = 0);
        });
    }
}

const app = new App();