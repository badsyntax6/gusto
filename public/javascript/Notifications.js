

class Notifications {
    constructor() {
        // console.log('Notifications Started');
        this.init();
    }

    async get() {
        const response = await fetch('/header/get-notifications');
        if (!response.redirected) {
            if (response.ok) return await response.json();
        }
    }

    async init() {
        const data = await this.get();
        const dropdown = document.querySelector('.notification-dropdown-menu');
        if (data && dropdown) {
            let num = 0;
            Object.values(data).forEach(v => {
                num++
                const color = this.adjustTypeColor(v.type);
                dropdown.innerHTML += `<li><div class="left"><span style="background-color:${color}"></span></div><div class="right"><button type="button">${v.message}</button><em>${v.time_ago}</em></div></li>`;                
            });
            document.querySelector('.btn-notification').innerHTML += `<span>${num}</span>`;
        }
    }

    adjustTypeColor(type) {
        switch (type) {
            case 'General': return '#1158a8'; break;
            case 'Warning': return '#d3b718'; break;
            case 'Error': return '#e23427'; break;
        }
    }
}

const notifier = new Notifications;