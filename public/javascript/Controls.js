class Controls {
    constructor() {
        this.url = window.location.pathname.split('/');
        this.view = this.url[1];
        // console.log('Controls Started');
    }

    userControls() {
        const group = document.querySelector('#user-group');
        group.addEventListener('change', () => {
            const ids = this.getCheckedBoxes();
            this.editUser(ids, group.value);
        });
    }

    async editUser(ids, group) {
        this.clearAlerts();
        const post = new URLSearchParams({ids: ids, group: group});
        const response = await fetch('/users/edit', {
            method: 'POST',
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            },
            body: post
        });  

        if (response.ok) {
            const data = await response.json();
            document.querySelector('#group-none').selected = true;
            document.querySelectorAll('.btn-list-control').forEach(element => element.disabled = true);
            if (data) {
                const {alert, message} = data;
                document.querySelector('.alert-area')
                .insertAdjacentHTML('afterbegin', `<div class="alert ${alert}"><strong>${alert}!</strong>${message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>`);
                pagination.renderTable();
            }
        } else return response;
    }

    delete() {
        this.clearAlerts();
        document.querySelector('.btn-mass-delete').addEventListener('click', async () => {
            const ids = this.getCheckedBoxes();
            const post = new URLSearchParams({ids: ids});
            const response = await fetch(`/${this.view}/delete`, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                body: post
            });  

            if (response.ok) {
                const data = await response.json();
                if (data) {
                    const {alert, message} = data;
                    document.querySelector('.alert-area')
                    .insertAdjacentHTML('afterbegin', `<div class="alert ${alert}"><strong>${alert}!</strong>${message} <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>`);
                    pagination.renderTable();
                }
            }
        });
    }

    getCheckedBoxes() {
        const checks = document.querySelectorAll('.checkbox');
        let ids = [];
        checks.forEach(v => { if (v.checked) ids.push(v.value); });
        return ids;
    }

    clearAlerts() {
        const alert = document.querySelector('.alert');
        if (alert) alert.remove();
    }
}

const controls = new Controls();