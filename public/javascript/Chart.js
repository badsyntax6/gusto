
class Chart {
    constructor() {
        console.log('Chart Started');
    }

    constructor(settings) {
        this.settings = extend({
            canvas: 'canvas',
            data: {},
            colors: ["#0061a1", "#c23e3e", "#4a9150", "#8b4c9e", '#e98a1e', '#6dc7ca', '#cbce34', '#f19595'],
            doughnut: 0,
            background: '#fff'
        }, settings);

        this.canvas = document.querySelector(settings.canvas);
        this.canvas.width = 300;
        this.canvas.height = 300;
        this.ctx = this.canvas.getContext('2d');
        this.draw();
    }

    drawPieSlice(ctx, centerX, centerY, radius, start_angle, end_angle, color) {
        ctx.beginPath();
        ctx.moveTo(centerX, centerY);
        ctx.arc(centerX, centerY, radius, start_angle, end_angle);
        ctx.closePath();
        ctx.fillStyle = color;
        ctx.strokeStyle = this.settings.background;
        ctx.lineWidth = 1;
        ctx.save();
        ctx.clip();
        ctx.fill();
        ctx.stroke();
        ctx.restore();
    }

    draw() {
        const {data, colors, doughnut, background} = this.settings;
        let total_value = 0;
        let color_index = 0;

        const sdata = sort(data, 'desc');

        for (let category in sdata) {
            let val = sdata[category];
            total_value += val;
        }
 
        let start_angle = 0;

        for (let category in sdata) {
            let val = sdata[category];
            let slice_angle = 2 * Math.PI * val / total_value;
            
            this.drawPieSlice(
                this.ctx,
                this.canvas.width / 2,
                this.canvas.height / 2,
                Math.min(this.canvas.width / 2, this.canvas.height / 2),
                start_angle,
                start_angle + slice_angle,
                colors[color_index%colors.length]
            );
 
            start_angle += slice_angle;
            color_index++;
        }

        if (doughnut) {
            this.drawPieSlice(
                this.ctx,
                this.canvas.width / 2,
                this.canvas.height / 2,
                doughnut * Math.min(this.canvas.width / 2, this.canvas.height / 2), 0, 2 * Math.PI, background
            );
        }

        start_angle = 0;

        for (let category in sdata) {
            let val = sdata[category];
            let slice_angle = 2 * Math.PI * val / total_value;
            var pie_radius = Math.min(this.canvas.width / 2, this.canvas.height / 2);
            var labelX = this.canvas.width / 2 + (10 + pie_radius / 2) * Math.cos(start_angle + slice_angle / 2);
            var labelY = this.canvas.height / 2 + (pie_radius / 2) * Math.sin(start_angle + slice_angle / 2);
        
            if (doughnut) {
                var offset = (pie_radius * doughnut) / 2;
                labelX = this.canvas.width / 2 + (offset + pie_radius / 2) * Math.cos(start_angle + slice_angle / 2);
                labelY = this.canvas.height / 2 + (offset + pie_radius / 2) * Math.sin(start_angle + slice_angle / 2);               
            }
        
            var label_text = Math.round(100 * val / total_value);
            this.ctx.fillStyle = background;
            this.ctx.textAlign = 'center';
            this.ctx.font = "12px Arial";
            if (label_text > 0) this.ctx.fillText(label_text + '%', labelX, labelY);
            start_angle += slice_angle;
        }
    }
}