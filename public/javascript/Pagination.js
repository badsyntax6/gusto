class Pagination {
    constructor() {
        // console.log('Pagination Started');
    }

    async init(settings) {
        this.settings = this.extend({
            wrapper: '.pagination-table',
            url: '',
            on_render: {},
        }, settings);
        this.data = {};
        this.prev_btns = document.querySelectorAll('.pagination-controls .btn-prev');
        this.next_btns = document.querySelectorAll('.pagination-controls .btn-next');
        this.selects = document.querySelectorAll('.pagination-select');

        this.goToPage();
        this.goPrev();
        this.goNext();
        this.search();

        try {
            return this.renderTable();
        } catch (error) {
            return error;
        }  
    }

    async renderTable() {
        delete this.data.search_keys;
        const loader = document.querySelector('.loading');
        if (loader) loader.style.cssText = 'display:block;';
        const inputs = new URLSearchParams(this.data);
        const {url, wrapper, on_render} = this.settings;
        const response = await fetch(url, {
            method: 'POST',
            body: inputs
        });

        let data = null;

        if (response.ok) {
            data = await response.json();
            
            if (loader) loader.style.cssText = 'display:none;';
            this.data = data;
            document.querySelector(wrapper).innerHTML = data.list;

            if (this.data.total_records == 0) {
                document.querySelector('table').remove();
                document.querySelector('.pagination-table').insertAdjacentHTML('afterBegin', '<span class="row mv25 pt20">No records found.</span>');
                if (loader) loader.remove();  
                return true;
            }
        
            this.numberRows();
            this.renderControls();
            this.renderTotalRecords();
            this.renderPageOf();
            this.sortRecords();
            this.checkAll();
            this.enableControls();
            this.limitPageRecords();

            Object.keys(on_render).forEach(key => {
                var func = on_render[key];
                if (typeof func === 'function' && key) {
                    try {
                        func();
                    } catch (error) {
                        return error;
                    }
                }
            });
            return data;
        } else {
            return response;
        }
    }

    numberRows() {
        const rows = document.querySelectorAll('.data-row');
        rows.forEach((v, k) => {
            let num = parseInt(k) + parseInt(this.data.start);
            v.querySelector('.number-col').textContent = ++num;
        });
    }

    renderControls() {
        // Enable / disable previous button if off / on first page
        if (this.data.page == 1) this.prev_btns.forEach(element => element.disabled = true);
        else this.prev_btns.forEach(element => element.disabled = false);
        // Enable / disable next button if off / on last page
        if (this.data.page == this.data.total_pages) this.next_btns.forEach(element => element.disabled = true);
        else this.next_btns.forEach(element => element.disabled = false);
        // Add page links with page numbers        
        this.selects.forEach(element => { 
            element.innerHTML = '';
            for (let i = 1; i <= this.data.total_pages; i++) element.insertAdjacentHTML('beforeEnd', `<option value="${i}">${i}</option>`);
        });
        // Pre-select the current page
        document.querySelectorAll('.pagination-select option').forEach(element => {
            if (parseInt(element.value) == this.data.page) element.parentElement.value = this.data.page;
        });
    }

    renderTotalRecords() {
        document.querySelectorAll('.total').forEach(element => element.textContent = 'Total: ' + this.data.total_records);
    }

    renderPageOf() {
        const page_of = document.querySelectorAll('.page-of');
        if (this.data.total_pages) page_of.forEach(element => element.textContent = 'Page: ' + this.data.page + ' of ' + this.data.total_pages);
        else page_of.forEach(element => element.textContent = 'Page: ' + 0 + ' of ' + 0);
    }

    goToPage() {
        this.selects.forEach(element => {
            element.addEventListener('change', () => {
                this.data.page = parseInt(element.value);
                this.renderTable();
            });
        });
    }

    goPrev() {
        this.prev_btns.forEach(element => {
            element.addEventListener('click', () => {
                this.data.page = --this.data.page;
                this.renderTable();
            });
        });
    }

    goNext() {
        this.next_btns.forEach(element => {
            element.addEventListener('click', () => {
                this.data.page = ++this.data.page;
                this.renderTable();
            });
        });
    }

    limitPageRecords() {
        const limit = document.querySelectorAll('.limit-per-page');
        limit.forEach(element => {
            element.value = this.data.limit;
            element.addEventListener('change', () => {
                this.data.page = 1;
                this.data.limit = element.value;
                this.renderTable();
            }, {once: true});
        });
    }

    search() {
        const btn = document.querySelectorAll('.btn-list-search');
        btn.forEach(element => {
            element.addEventListener('click', (e) => {
                const search = e.target.parentElement.querySelector('.pagination-search');
                this.data.search_string = search.value;
                this.data.page = 1;
                this.renderTable();  
            });
        });
    }

    sortRecords() {
        const sort_btns = document.querySelectorAll('.btn-sort');
        sort_btns.forEach(element => {
            element.addEventListener('click', () => {
                this.data.orderby = element.textContent.toLowerCase().replace(/ /g, '_');
                if (this.data.direction === 'asc') this.data.direction = 'desc';
                else this.data.direction = 'asc';
                this.renderTable();
            });
        });
    }

    checkAll() {
        const check_all = document.querySelector('#check-all');
        const checkboxes = document.querySelectorAll('.checkbox');
        check_all.addEventListener('click', () => {
            if (check_all.checked) checkboxes.forEach(element => element.checked = true);
            else checkboxes.forEach(element => element.checked = false);
        });
    }

    enableControls() {
        const checkboxes = document.querySelectorAll('.checkbox, #check-all');
        const controls = document.querySelectorAll('.btn-list-control');
        checkboxes.forEach(element => {
            element.addEventListener('change', () => {
                if (element.checked) controls.forEach(element => element.disabled = false);
                else controls.forEach(element => element.disabled = true);
            });
        });
    }

    extend(defaults, settings) {
        for (let prop in settings) {
            if (typeof settings[prop] === 'object') {
                defaults[prop] = this.extend(defaults[prop], settings[prop]);
            } else {
                defaults[prop] = settings[prop];
            }
        }
        return defaults;
    }
}

const pagination = new Pagination();