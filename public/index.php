<?php 

// Define Gusto version.
define('BUILD', '2020-04-11.2:02');

// Include Gusto config.
require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/../private/config/settings.php';

// Include start files.
require_once PRIVATE_DIR . '/autoload.php';

set_error_handler(function($num, $err, $file, $line) {
    if ($num) {
        $error = $err . ' in ' . $file . ' on line ' . $line . "\n";
        Log::server($error);
        echo $error . '<br>';
    }
});

register_shutdown_function(function() {
    $e = error_get_last();
    if ($e['type'] === E_ERROR) Log::server($e['message'] . ' in ' . $e['file'] . ' on line ' . $e['line'] . "\n");
});

// Start App
App::init();